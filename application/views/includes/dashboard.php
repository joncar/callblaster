<div class="row">
    <div class="space-6"></div>

    <div class="col-sm-7 infobox-container">
        <div class="infobox infobox-green">
            <div class="infobox-icon">
                <i class="ace-icon fa fa-user"></i>
            </div>

            <div class="infobox-data">
                <?php $this->db->join('agenda','agenda.id = llamadas.agenda_id'); ?>
                <span class="infobox-data-number"><?= $this->db->get_where('llamadas',array('user_id'=>$this->user->id))->num_rows() ?></span>
                <div class="infobox-content">Llamadas completadas</div>
            </div>
        </div>

        <div class="infobox infobox-blue">
            <div class="infobox-icon">
                <i class="ace-icon fa fa-university"></i>
            </div>

            <div class="infobox-data">
                <?php $this->db->join('agenda','agenda.id = llamadas.agenda_id'); ?>
                <span class="infobox-data-number"><?= $this->db->get_where('llamadas',array('user_id'=>$this->user->id))->num_rows() ?></span>
                <div class="infobox-content">Llamadas no contestadas</div>
            </div>
        </div>

        <div class="infobox infobox-blue">
            <div class="infobox-icon">
                <i class="ace-icon fa fa-university"></i>
            </div>

            <div class="infobox-data">
                <span class="infobox-data-number"><?= $this->db->query('SELECT FORMAT(SUM(precio_venta),0,"de_DE") as total from llamadas INNER JOIN agenda ON agenda.id = llamadas.agenda_id where user_id='.$this->user->id)->row()->total ?></span>
                <div class="infobox-content">Dinero Gastado</div>
            </div>
        </div>

        <div class="infobox infobox-blue">
            <div class="infobox-icon">
                <i class="ace-icon fa fa-university"></i>
            </div>

            <div class="infobox-data">
                <span class="infobox-data-number"><?= $this->user->saldo ?></span>
                <div class="infobox-content">Dinero Restante</div>
            </div>
        </div>                   
    </div>

    <div class="vspace-12-sm"></div>

    <div class="col-sm-5">
        <div class="widget-box">
            <div class="widget-header widget-header-flat widget-header-small">
                <h5 class="widget-title">
                    <i class="ace-icon fa fa-signal"></i>
                    Efectividad de las campañas
                </h5>
            </div>

            <div class="widget-body">
                <div class="widget-main">
                        <div id="piechart-placeholder" style="width: 90%; min-height: 150px; padding: 0px; position: relative;">
                            <canvas class="flot-base" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 390px; height: 150px;" width="390" height="150"></canvas>
                            <canvas class="flot-overlay" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 390px; height: 150px;" width="390" height="150"></canvas>
                            <div class="legend">
                                <div style="position: absolute; width: 93px; height: 110px; top: 15px; right: -30px; background-color: rgb(255, 255, 255); opacity: 0.85;"> 
                                </div>
                                <table style="position:absolute;top:15px;right:-30px;;font-size:smaller;color:#545454">
                                    <tbody>
                                        <tr>
                                            <td class="legendColorBox">
                                                <div style="border:1px solid null;padding:1px">
                                                    <div style="width:4px;height:0;border:5px solid #68BC31;overflow:hidden"></div>                                                    
                                                </div>
                                            </td>
                                            <td class="legendLabel">Llamadas Contestadas</td>
                                        </tr>
                                        <tr>
                                            <td class="legendColorBox">
                                                <div style="border:1px solid null;padding:1px">
                                                    <div style="width:4px;height:0;border:5px solid #2091CF;overflow:hidden"></div>

                                                </div>
                                            </td>
                                            <td class="legendLabel">Llamadas no contestadas</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="hr hr8 hr-double"></div>  
                        <div class="clearfix">
                        <?php //foreach($creditos->result() as $c): ?>
                            <!--<div class="grid3">
                                <span class="grey">
                                    <?= $c->tipo_credito_nombre ?>
                                </span>
                                <h4 class="bigger pull-right"><?= $c->tipos ?></h4>
                            </div>-->
                        <?php //endforeach ?>
                    </div>
                    </div>
                </div><!-- /.widget-main -->
            </div><!-- /.widget-body -->
        </div><!-- /.widget-box -->
    </div><!-- /.col -->
</div>

<div class="row">
    <div class="col-sm-5">
        <div class="widget-box transparent">
            <div class="widget-header widget-header-flat">
                <h4 class="widget-title lighter">
                    <i class="ace-icon fa fa-star orange"></i>
                    Ultimas llamadas
                </h4>

                <div class="widget-toolbar">
                    <a href="#" data-action="collapse">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>

            <div class="widget-body">
                <div class="widget-main no-padding">
                    <table class="table table-bordered table-striped">
                        <thead class="thin-border-bottom">
                            <tr>
                                <th>
                                    <i class="ace-icon fa fa-caret-right blue"></i>Desde
                                </th>
                                <th>
                                    <i class="ace-icon fa fa-caret-right blue"></i>Para
                                </th>
                                <th class="hidden-480">
                                    <i class="ace-icon fa fa-caret-right blue"></i>Duracion
                                </th>
                                <th class="hidden-480">
                                    <i class="ace-icon fa fa-caret-right blue"></i>Respondido por
                                </th>
                                <th class="hidden-480">
                                    <i class="ace-icon fa fa-caret-right blue"></i>Costo
                                </th>
                                <th class="hidden-480">
                                    <i class="ace-icon fa fa-caret-right blue"></i>Status
                                </th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php foreach($last->result() as $l): ?>
                                <tr>                                    
                                    <td>                                        
                                        <b class="green"><?= $l->mascara_numero ?></b>
                                    </td>
                                    <td class="hidden-480">
                                        <span class="label label-info arrowed-right arrowed-in">
                                            <?= $l->telefono ?>
                                        </span>
                                    </td>
                                    <td class="hidden-480">
                                        <span class="label label-info arrowed-right arrowed-in">
                                            <?= $l->duracion ?>
                                        </span>
                                    </td>
                                    <td class="hidden-480">
                                        <span class="label label-info arrowed-right arrowed-in">
                                            <?= $l->respondido_por ?>
                                        </span>
                                    </td>
                                    <td class="hidden-480">
                                        <span class="label label-info arrowed-right arrowed-in">
                                            <?= $l->precio ?>
                                        </span>
                                    </td>
                                    <td class="hidden-480">
                                        <span class="label label-info arrowed-right arrowed-in">
                                            <?= $l->status ?>
                                        </span>
                                    </td>
                                </tr>   
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div><!-- /.widget-main -->
            </div><!-- /.widget-body -->
        </div><!-- /.widget-box -->
    </div><!-- /.col -->

    <div class="col-sm-7">
        <div class="widget-box transparent">
            <div class="widget-header widget-header-flat">
                <h4 class="widget-title lighter">
                    <i class="ace-icon fa fa-signal"></i>
                    Relación Pago-crédito
                </h4>

                <div class="widget-toolbar">
                    <a href="#" data-action="collapse">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>

            <div class="widget-body">
                <div class="widget-main padding-4">
                    <div id="sales-charts" style="width: 100%; height: 220px; padding: 0px; position: relative;"><canvas class="flot-base" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 644px; height: 220px;" width="644" height="220"></canvas><div class="flot-text" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; font-size: smaller; color: rgb(84, 84, 84);"><div class="flot-x-axis flot-x1-axis xAxis x1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;"><div style="position: absolute; max-width: 80px; top: 204px; left: 29px; text-align: center;" class="flot-tick-label tickLabel">0.0</div><div style="position: absolute; max-width: 80px; top: 204px; left: 125px; text-align: center;" class="flot-tick-label tickLabel">1.0</div><div style="position: absolute; max-width: 80px; top: 204px; left: 222px; text-align: center;" class="flot-tick-label tickLabel">2.0</div><div style="position: absolute; max-width: 80px; top: 204px; left: 319px; text-align: center;" class="flot-tick-label tickLabel">3.0</div><div style="position: absolute; max-width: 80px; top: 204px; left: 416px; text-align: center;" class="flot-tick-label tickLabel">4.0</div><div style="position: absolute; max-width: 80px; top: 204px; left: 512px; text-align: center;" class="flot-tick-label tickLabel">5.0</div><div style="position: absolute; max-width: 80px; top: 204px; left: 609px; text-align: center;" class="flot-tick-label tickLabel">6.0</div></div><div class="flot-y-axis flot-y1-axis yAxis y1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;"><div style="position: absolute; top: 192px; left: 1px; text-align: right;" class="flot-tick-label tickLabel">-2.000</div><div style="position: absolute; top: 168px; left: 1px; text-align: right;" class="flot-tick-label tickLabel">-1.500</div><div style="position: absolute; top: 144px; left: 1px; text-align: right;" class="flot-tick-label tickLabel">-1.000</div><div style="position: absolute; top: 120px; left: 1px; text-align: right;" class="flot-tick-label tickLabel">-0.500</div><div style="position: absolute; top: 96px; left: 4px; text-align: right;" class="flot-tick-label tickLabel">0.000</div><div style="position: absolute; top: 72px; left: 4px; text-align: right;" class="flot-tick-label tickLabel">0.500</div><div style="position: absolute; top: 48px; left: 4px; text-align: right;" class="flot-tick-label tickLabel">1.000</div><div style="position: absolute; top: 24px; left: 4px; text-align: right;" class="flot-tick-label tickLabel">1.500</div><div style="position: absolute; top: 1px; left: 4px; text-align: right;" class="flot-tick-label tickLabel">2.000</div></div></div><canvas class="flot-overlay" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 644px; height: 220px;" width="644" height="220"></canvas><div class="legend"><div style="position: absolute; width: 64px; height: 66px; top: 13px; right: 13px; background-color: rgb(255, 255, 255); opacity: 0.85;"> </div><table style="position:absolute;top:13px;right:13px;;font-size:smaller;color:#545454"><tbody><tr><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px"><div style="width:4px;height:0;border:5px solid rgb(237,194,64);overflow:hidden"></div></div></td><td class="legendLabel">Domains</td></tr><tr><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px"><div style="width:4px;height:0;border:5px solid rgb(175,216,248);overflow:hidden"></div></div></td><td class="legendLabel">Hosting</td></tr><tr><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px"><div style="width:4px;height:0;border:5px solid rgb(203,75,75);overflow:hidden"></div></div></td><td class="legendLabel">Services</td></tr></tbody></table></div></div>
                </div><!-- /.widget-main -->
            </div><!-- /.widget-body -->
        </div><!-- /.widget-box -->
    </div><!-- /.col -->
</div>
<script type="text/javascript">
    jQuery(function ($) {
        $('.easy-pie-chart.percentage').each(function () {
            var $box = $(this).closest('.infobox');
            var barColor = $(this).data('color') || (!$box.hasClass('infobox-dark') ? $box.css('color') : 'rgba(255,255,255,0.95)');
            var trackColor = barColor == 'rgba(255,255,255,0.95)' ? 'rgba(255,255,255,0.25)' : '#E2E2E2';
            var size = parseInt($(this).data('size')) || 50;
            $(this).easyPieChart({
                barColor: barColor,
                trackColor: trackColor,
                scaleColor: false,
                lineCap: 'butt',
                lineWidth: parseInt(size / 10),
                animate: ace.vars['old_ie'] ? false : 1000,
                size: size
            });
        })

        $('.sparkline').each(function () {
            var $box = $(this).closest('.infobox');
            var barColor = !$box.hasClass('infobox-dark') ? $box.css('color') : '#FFF';
            $(this).sparkline('html',
                    {
                        tagValuesAttribute: 'data-values',
                        type: 'bar',
                        barColor: barColor,
                        chartRangeMin: $(this).data('min') || 0
                    });
        });


        //flot chart resize plugin, somehow manipulates default browser resize event to optimize it!
        //but sometimes it brings up errors with normal resize event handlers
        $.resize.throttleWindow = false;

        var placeholder = $('#piechart-placeholder').css({'width': '90%', 'min-height': '150px'});
        var data = [
            {label: "Llamadas contestadas", data: <?= $llamadas->row(0)->cantidad ?>, color: "#68BC31"},
            {label: "Llamadas no contestadas", data: <?= $llamadas->row(0)->cantidad ?>, color: "#2091CF"},
        ];
        function drawPieChart(placeholder, data, position) {
            $.plot(placeholder, data, {
                series: {
                    pie: {
                        show: true,
                        tilt: 0.8,
                        highlight: {
                            opacity: 0.25
                        },
                        stroke: {
                            color: '#fff',
                            width: 2
                        },
                        startAngle: 2
                    }
                },
                legend: {
                    show: true,
                    position: position || "ne",
                    labelBoxBorderColor: null,
                    margin: [-30, 15]
                }
                ,
                grid: {
                    hoverable: true,
                    clickable: true
                }
            })
        }
        drawPieChart(placeholder, data);

        /**
         we saved the drawing function and the data to redraw with different position later when switching to RTL mode dynamically
         so that's not needed actually.
         */
        placeholder.data('chart', data);
        placeholder.data('draw', drawPieChart);


        //pie chart tooltip example
        var $tooltip = $("<div class='tooltip top in'><div class='tooltip-inner'></div></div>").hide().appendTo('body');
        var previousPoint = null;

        placeholder.on('plothover', function (event, pos, item) {
            if (item) {
                if (previousPoint != item.seriesIndex) {
                    previousPoint = item.seriesIndex;
                    var tip = item.series['label'] + " : " + item.series['percent'] + '%';
                    $tooltip.show().children(0).text(tip);
                }
                $tooltip.css({top: pos.pageY + 10, left: pos.pageX + 10});
            } else {
                $tooltip.hide();
                previousPoint = null;
            }

        });

        /////////////////////////////////////
        $(document).one('ajaxloadstart.page', function (e) {
            $tooltip.remove();
        });




        var d1 = [];
        for (var i = 0; i < Math.PI * 2; i += 0.5) {
            d1.push([i, Math.sin(i)]);
        }

        var d2 = [];
        for (var i = 0; i < Math.PI * 2; i += 0.5) {
            d2.push([i, Math.cos(i)]);
        }

        var d3 = [];
        for (var i = 0; i < Math.PI * 2; i += 0.2) {
            d3.push([i, Math.tan(i)]);
        }


        var sales_charts = $('#sales-charts').css({'width': '100%', 'height': '220px'});
        $.plot("#sales-charts", [
            {label: "Domains", data: d1},
            {label: "Hosting", data: d2},
            {label: "Services", data: d3}
        ], {
            hoverable: true,
            shadowSize: 0,
            series: {
                lines: {show: true},
                points: {show: true}
            },
            xaxis: {
                tickLength: 0
            },
            yaxis: {
                ticks: 10,
                min: -2,
                max: 2,
                tickDecimals: 3
            },
            grid: {
                backgroundColor: {colors: ["#fff", "#fff"]},
                borderWidth: 1,
                borderColor: '#555'
            }
        });


        $('#recent-box [data-rel="tooltip"]').tooltip({placement: tooltip_placement});
        function tooltip_placement(context, source) {
            var $source = $(source);
            var $parent = $source.closest('.tab-content')
            var off1 = $parent.offset();
            var w1 = $parent.width();

            var off2 = $source.offset();
            //var w2 = $source.width();

            if (parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2))
                return 'right';
            return 'left';
        }


        $('.dialogs,.comments').ace_scroll({
            size: 300
        });


        //Android's default browser somehow is confused when tapping on label which will lead to dragging the task
        //so disable dragging when clicking on label
        var agent = navigator.userAgent.toLowerCase();
        if (ace.vars['touch'] && ace.vars['android']) {
            $('#tasks').on('touchstart', function (e) {
                var li = $(e.target).closest('#tasks li');
                if (li.length == 0)
                    return;
                var label = li.find('label.inline').get(0);
                if (label == e.target || $.contains(label, e.target))
                    e.stopImmediatePropagation();
            });
        }

        $('#tasks').sortable({
            opacity: 0.8,
            revert: true,
            forceHelperSize: true,
            placeholder: 'draggable-placeholder',
            forcePlaceholderSize: true,
            tolerance: 'pointer',
            stop: function (event, ui) {
                //just for Chrome!!!! so that dropdowns on items don't appear below other items after being moved
                $(ui.item).css('z-index', 'auto');
            }
        }
        );
        $('#tasks').disableSelection();
        $('#tasks input:checkbox').removeAttr('checked').on('click', function () {
            if (this.checked)
                $(this).closest('li').addClass('selected');
            else
                $(this).closest('li').removeClass('selected');
        });


        //show the dropdowns on top or bottom depending on window height and menu position
        $('#task-tab .dropdown-hover').on('mouseenter', function (e) {
            var offset = $(this).offset();

            var $w = $(window)
            if (offset.top > $w.scrollTop() + $w.innerHeight() - 100)
                $(this).addClass('dropup');
            else
                $(this).removeClass('dropup');
        });

    })
</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
            (function (d, w, c) {
                (w[c] = w[c] || []).push(function () {
                    try {
                        w.yaCounter25836836 = new Ya.Metrika({id: 25836836,
                            webvisor: true,
                            clickmap: true,
                            trackLinks: true,
                            accurateTrackBounce: true});
                    } catch (e) {
                    }
                });

                var n = d.getElementsByTagName("script")[0],
                        s = d.createElement("script"),
                        f = function () {
                            n.parentNode.insertBefore(s, n);
                        };
                s.type = "text/javascript";
                s.async = true;
                s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

                if (w.opera == "[object Opera]") {
                    d.addEventListener("DOMContentLoaded", f, false);
                } else {
                    f();
                }
            })(document, window, "yandex_metrika_callbacks");
</script>
