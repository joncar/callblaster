<?php if($this->user->log): ?>
<div id="sidebar" class="sidebar responsive">
        <ul class="nav nav-list">
            <li class="active highlight">
                <a href="<?= site_url('panel') ?>">
                        <i class="menu-icon fa fa-tachometer"></i>
                        <span class="menu-text">Escritorio</span>
                </a>
                <b class="arrow"></b>
            </li>
             <!--- Alumnos --->
             <?php 
                    $menu = array(                        
                        'agenda'=>array('admin/agenda','admin/files'),
                        'campanas'=>array('admin/campanas'),
                        'notificaciones'=>array('admin/notificaciones'),
                        'b'=>array('blog_categorias','blog'),                        
                        'paginas'=>array('admin/paginas','admin/subscriptores'), 
                        'balance'=>array('admin/balance','admin/balance/add'),
                        'admin'=>array('ajustes','llamadas','campanias','contactos'),
                        'seguridad'=>array('grupos','funciones','user')
                    );
                    $menu = $this->user->filtrarMenu($menu);
                    $label = array(      
                        'notificaciones'=>array('Notificaciones','fa fa-envelope'),
                        'add'=>array('Recargar'),
                        'files'=>array('Importar contactos'),
                        'balance'=>array('Mi Balance','fa fa-money'),
                        'campanas'=>array('Mis Campañas','fa fa-bullhorn'),
                        'campanias'=>array('Campañas','fa fa-bullhorn'),
                        'b'=>array('Blog','fa fa-book'),                        
                        'agenda'=>array('Agenda','fa fa-address-card-o'),
                        'paginas'=>array('Paginas','fa fa-file-powerpoint-o'),                        
                        'seguridad'=>array('Seguridad','fa fa-user-secret')
                    );
             ?>
             <?php  echo getMenu($menu,$label); ?>            
        </ul>
       <div id="sidebar-collapse" class="sidebar-toggle sidebar-collapse">
            <i data-icon2="ace-icon fa fa-angle-double-right" data-icon1="ace-icon fa fa-angle-double-left" class="ace-icon fa fa-angle-double-left"></i>
        </div>
        <div style="color:white; background:#222222; font-size:8px; text-align:center">
            <a href="#" style="color:white;">EVA software</a>
        </div>
        <script type="text/javascript">
                try{ace.settings.check('sidebar' , 'collapsed')
                ace.settings.sidebar_collapsed(true, true);
                }catch(e){}
        </script>
</div>
<?php endif ?>
