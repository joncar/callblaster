<!-- FOOTER ============================================= -->	
<footer id="footer" class="bg-dark footer division">
    <!-- PRE-FOOTER -->
    <div id="pre-footer" class="p-top-80 p-bottom-40">
        <div class="container">
            <div class="row">
                <!-- FOOTER ABOUT WIDGET -->
                <div class="col-md-3 footer-about-widget m-bottom-40">	
                    <!-- Title -->
                    <h4 class="h4-lg">About Altron</h4>	
                    <!-- Text -->
                    <p>Donec vel sapien augue. Integer urna turpis cursus porta, mauris molestie</p>
                    <p>Cursus sodales vitae suscipit egestas lobortis aliquam quaerat hendrerit sapien undo euismod blandit.
                        nulla at sodales mollis in hendrerit tellus
                    </p>
                </div>
                <!-- FOOTER NEWS WIDGET -->
                <div class="col-md-3 p-right-30 footer-news-widget m-bottom-40">	
                    <!-- Title -->
                    <h4 class="h4-lg">Recent News</h4>
                    <ul class="footer-news clearfix">
                        <li><a class="foo-news" href="#">Cursus sodales vitae augue egestas volutpat sodales vitae rutrum <span>May 20, 2016</span></a></li>
                        <li><a class="foo-news" href="#">Dapibus egestas lobortis magna phasellus nec euismod<span>May 08, 2016</span></a></li>							
                    </ul>
                </div>
                <!-- FOOTER SOCIALS WIDGET -->
                <div class="col-md-2 footer-socials-widget m-bottom-40">		
                    <!-- Title -->
                    <h4 class="h4-lg">Follow Us</h4>	
                    <!-- Icons list -->
                    <ul class="footer-icons clearfix">
                        <li><a class="foo-social ico-facebook" href="#"><i class="fa fa-facebook" aria-hidden="true"></i> Facebook</a></li>
                        <li><a class="foo-social ico-twitter" href="#"><i class="fa fa-twitter" aria-hidden="true"></i> Twitter</a></li>
                        <li><a class="foo-social ico-behance" href="#"><i class="fa fa-behance" aria-hidden="true"></i> Behance</a></li>
                        <li><a class="foo-social ico-linkedin" href="#"><i class="fa fa-linkedin" aria-hidden="true"></i> Linkedin</a></li>									
                        <li><a class="foo-social ico-instagram" href="#"><i class="fa fa-instagram" aria-hidden="true"></i> Instagram</a></li>
                    </ul>
                </div>
                <!-- FOOTER NEWSLETTER WIDGET -->
                <div class="col-md-4 footer-newsletter-widget m-bottom-40">	
                    <!-- Title -->
                    <h4 class="h4-lg">Subscribe To Our Newsletter</h4>
                    <p>Be amongst the first to know about our news, ideas, updates and latest features</p>
                    <!-- Newsletter Form Input -->
                    <form class="newsletter-form m-top-20">
                        <div class="input-group">
                            <input type="email" class="form-control" placeholder="Email Address" required id="s-email">								
                            <span class="input-group-btn"><button type="submit" class="btn"><span class="ti-email"></span></button></span>
                        </div>
                        <!-- Newsletter Form Notification -->		
                        <label for="s-email" id="form-notification"></label>
                    </form>
                    <p class="no-spam">* Please trust us, we will never send you spam </p>
                </div>
            </div>	  <!-- Eтd row -->
        </div>	  <!-- End container -->
    </div>	  <!-- END PRE-FOOTER -->
    <div class="container footer-bottom p-top-20 p-bottom-20">	
        <div class="row">
            <!-- FOOTER COPYRIGHT -->
            <div id="footer-copyright" class="col-sm-6 col-md-6">	
                <p>&copy; 2016 PowerNode. All Rights Reserved</p>
            </div>	
            <!-- FOOTER LINKS -->
            <div id="footer-links" class="col-sm-6 col-md-6 text-right">	
                <!-- Links List -->
                <ul class="footer-links clearfix">
                    <li><a class="foo-link" href="#">Privacy Policy</a></li>
                    <li><a class="foo-link" href="#">Terms of Service</a></li>
                    <li><a class="foo-link" href="#">Sitemap</a></li>
                </ul>
            </div>
        </div>	<!-- End row -->
    </div>	<!-- End container -->
</footer>	<!-- END FOOTER -->	
