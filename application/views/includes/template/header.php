    <!-- HEADER 
    ============================================= -->
    <header id="header" class="no-bg header">		
        <div class="navbar navbar-fixed-top">	
            <div class="container">
                <!-- Navigation Bar -->
                <div class="navbar-header">
                    <!-- Responsive Menu Button -->
                    <button type="button" id="nav-toggle" class="navbar-toggle text-right" data-toggle="collapse" data-target="#navigation-menu">
                        <span class="sr-only">Toggle navigation</span> 
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- LOGO IMAGE -->
                    <!-- Recommended sizes 150x22px; -->
                    <a class="navbar-brand logo-white" href="<?= site_url() ?>"><img src="<?= base_url() ?>img/logo-white.png" alt="logo"></a>
                    <a class="navbar-brand logo-black" href="<?= site_url() ?>"><img src="<?= base_url() ?>img/logo.png" alt="logo"></a>
                </div>	<!-- End Navigation Bar -->
                <!-- Navigation Menu -->
                <nav id="navigation-menu" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right editContent">													
                        <li><a href="#nosotros">Nosotros</a></li>															
                        <li><a href="#portfolio">Projects</a></li>
                        <li><a href="#pricing">Pricing</a></li>	
                        <li><a href="#services">Services</a></li>	
                        <li><a href="#team">Team</a></li>
                        <li><a href="#blog">Blog</a></li>	
                        <li><a href="#contacts">Contacts</a></li>
                        <li><a class="download" href="<?= site_url('panel') ?>">Registrate</a></li>								
                    </ul>
                </nav>  <!-- End Navigation Menu -->
            </div>	  <!-- End container -->
        </div>	   <!-- End navbar fixed top  -->
    </header>	<!-- END HEADER -->	
    <!-- INTRO-7
    ============================================= -->
