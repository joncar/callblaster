<!DOCTYPE html>
<!-- Altron - Multi-Purpose Landing Page Template design by DSAThemes (http://www.dsathemes.com) -->
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="author" content="DSAThemes">	
        <meta name="description" content="Altron - Multi-Purpose Landing Page Template"/>
        <meta name="keywords" content="Responsive, HTML5 template, DSAThemes, Multi-Purpose, One Page, Landing, Business, Creative, Corporate, Agency Template, Project">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- SITE TITLE -->
        <title><?= !empty($title)?$title:'' ?></title>
        <!-- FAVICON AND TOUCH ICONS  -->
        <link rel="shortcut icon" href="<?= base_url() ?>img/icons/favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?= base_url() ?>img/icons/favicon.ico" type="image/x-icon">
        <link rel="apple-touch-icon" sizes="144x144" href="<?= base_url() ?>img/icons/apple-touch-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?= base_url() ?>img/icons/apple-touch-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?= base_url() ?>img/icons/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" href="<?= base_url() ?>img/icons/apple-touch-icon.png">
        <!-- BOOTSTRAP CSS -->
        <link href="<?= base_url() ?>css/template/bootstrap.min.css" rel="stylesheet">
        <!-- FONT ICONS -->
        <link href="<?= base_url() ?>css/template/font-awesome.min.css" rel="stylesheet">	
        <link href="<?= base_url() ?>css/template/themify-icons.css" rel="stylesheet">
        <!-- GOOGLE FONTS -->
        <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700,900' rel='stylesheet' type='text/css'>
        <!-- PLUGINS STYLESHEET -->
        <link href="<?= base_url() ?>css/template/owl.carousel.css" rel="stylesheet">
        <link href="<?= base_url() ?>css/template/flexslider.css" rel="stylesheet">
        <link href="<?= base_url() ?>css/template/prettyPhoto.css" rel="stylesheet">
        <!-- ON SCROLL ANIMATION -->
        <link href="<?= base_url() ?>css/template/animate.min.css" rel="stylesheet">
        <!-- TEMPLATE CSS -->
        <link href="<?= base_url() ?>css/template/base.css" rel="stylesheet"> 	
        <link href="<?= base_url() ?>css/template/style.css" rel="stylesheet"> 	
        <!-- RESPONSIVE CSS -->
        <link href="<?= base_url() ?>css/template/responsive.css" rel="stylesheet"> 
    </head>
    <body>
        <!-- PRELOADER
        ============================================= -->		
        <div class="animationload">
            <div class="loader"></div>
        </div>
        <!-- PAGE CONTENT
        ============================================= -->	
        <div id="page" class="page-wrapper">
            <?php $this->load->view('includes/template/header'); ?>
            <?php $this->load->view($view); ?>
            <?php $this->load->view('includes/template/footer'); ?>
        </div>	<!-- END PAGE CONTENT -->
        <!-- EXTERNAL SCRIPTS
        ============================================= -->	
        <script src="<?= base_url() ?>js/template/jquery-2.2.4.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>js/template/bootstrap.min.js" type="text/javascript"></script>	
        <script src="<?= base_url() ?>js/template/modernizr.custom.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>js/template/jquery.easing.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>js/template/retina.js" type="text/javascript"></script>	
        <script src="<?= base_url() ?>js/template/jquery.stellar.min.js" type="text/javascript"></script>	
        <script src="<?= base_url() ?>js/template/jquery.scrollto.js"></script>
        <script defer src="<?= base_url() ?>js/template/jquery.appear.js"></script>
        <script defer src="<?= base_url() ?>js/template/jquery.vide.min.js"></script>		
        <script src="<?= base_url() ?>js/template/owl.carousel.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>js/template/jquery.prettyPhoto.js" type="text/javascript"></script>
        <script defer src="<?= base_url() ?>js/template/jquery.flexslider.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>js/template/waypoints.min.js" type="text/javascript"></script>	
        <script src="<?= base_url() ?>js/template/jquery.ajaxchimp.min.js"></script>
        <script src="<?= base_url() ?>js/template/contact_form.js" type="text/javascript"></script>	
        <script defer src="<?= base_url() ?>js/template/jquery.validate.min.js" type="text/javascript"></script>
        <!-- Custom Script -->		
        <script src="<?= base_url() ?>js/template/custom.js" type="text/javascript"></script>
    </body>
</html>
