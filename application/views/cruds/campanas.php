<?= $output ?>
<?php $this->load->view('predesign/datepicker'); ?>
<script>
    $("#field-fecha_desde").datetimepicker({
        timeFormat: "HH:mm",
        dateFormat: "dd/mm/yy",
        showButtonPanel: true,
        changeMonth: true,
        changeYear: true
    });
    $("#field-fecha_desde").button();
    $("#field-fecha_desde").click(function(){
            $(this).parent().find("#field-fecha_desde").val("");            
            return false;
    });	
    $("#field-fecha_desde").css("text-align",'left');
</script>