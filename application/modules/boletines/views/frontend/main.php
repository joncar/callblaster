<div class="destinations-page loaded" id="page-content"> <!-- blog page header with video bg --> 
        <header class="overlay">
            <?php $this->load->view('includes/template/menu2'); ?>             
        </header>
        <main>
            <section id="destinations-map">
                
            </section>
            <section class="regions">
                <div class="regions-inline">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-3 text-uppercase">
                                <h6>select your region</h6> 
                            </div> 
                            <div class="col-md-9"> 
                                <ul class="list-inline">
                                    <?php foreach($categorias->result() as $c): ?>
                                        <li> 
                                            <a class="text-uppercase" href="#"><?= $c->categorias_hoteles_nombre ?></a>
                                            <i class="result"><?= $c->cantidad ?></i> 
                                        </li>
                                    <?php endforeach ?>
                                </ul> 
                            </div> 
                        </div> 
                    </div> 
                </div>
                <div class="container"> 
                    <div class="row"> <!-- sidebar --> 
                        <?php $this->load->view('_categorias'); ?>

                        <div class="col-md-9 text-uppercase"> 
                            <div class="regions-block-intro"> 
                                <h1>all</h1> 
                                <i class="result">9</i> 
                                <span>results shown</span>
                                <span class="sown">abolisher</span>  
                                <div class="pull-right cs-select cs-skin-elastic adventures-select" tabindex="0">
                                    <select name="categorias_hoteles_id" class="pull-right cs-select cs-skin-elastic adventures-select" id="experience"> 
                                        <option selected="" disabled="" value="0">Ubicación</option> 
                                        <?php foreach($this->db->get_where('categorias_hoteles')->result() as $c): ?>
                                            <option value="<?= $c->id ?>"><?= $c->categorias_hoteles_nombre ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div> <!-- advenures blocks --> 
                            <ul class="list-inline text-uppercase adventure-block"> 
                                <?php foreach($detail->result() as $d): ?>
                                    <?php $this->load->view('frontend/_entry',array('detail'=>$d)); ?>
                                <?php endforeach ?>
                            </ul> 
                            <button class="text-uppercase show-more">show all adventures</button> 
                            <!-- /.advenures blocks --> 
                        </div>
                    </div> 
                </div> 
            </section>
            <?= $this->load->view('includes/template/contact') ?>
            <button class="btn goUp-btn"> 
                <i class="fa fa-angle-up"></i> 
                <span>Go Up</span>
                <span class="fitz">noninfantry</span> 
            </button> <!-- /.go up arrow --> 
        </main> <!-- /.main content --> <!-- footer content --> 
        <?= $this->load->view('includes/template/footer'); ?>
</div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBoW1mnkomGhsB2yL--AYoFdnE-jkgskSI" type="text/javascript"></script>
<script type="text/javascript" charset="UTF-8" src="//maps.googleapis.com/maps-api-v3/api/js/26/1/intl/es_ALL/common.js"></script>
<script type="text/javascript" charset="UTF-8" src="//maps.googleapis.com/maps-api-v3/api/js/26/1/intl/es_ALL/map.js"></script>
<script type="text/javascript" charset="UTF-8" src="//maps.googleapis.com/maps-api-v3/api/js/26/1/intl/es_ALL/util.js"></script>
<script type="text/javascript" charset="UTF-8" src="//maps.googleapis.com/maps-api-v3/api/js/26/1/intl/es_ALL/marker.js"></script>
<script type="text/javascript" charset="UTF-8" src="//maps.googleapis.com/maps-api-v3/api/js/26/1/intl/es_ALL/onion.js"></script>
<script type="text/javascript" charset="UTF-8" src="//maps.googleapis.com/maps-api-v3/api/js/26/1/intl/es_ALL/stats.js"></script>
<script type="text/javascript" charset="UTF-8" src="//maps.googleapis.com/maps-api-v3/api/js/26/1/intl/es_ALL/controls.js"></script>
<script type="text/javascript">
    google.maps.event.addDomListener(window, 'load', initMap); 
    function initMap() {
        var mapOptions = {
            zoom: 11, 
            scrollwheel: false,
            zoomControl: true, 
            mapTypeControl: false, 
            scaleControl: false, 
            streetViewControl: false, 
            center: new google.maps.LatLng(39.67442740076737,3.0157470703125), 
            styles: [
                { "featureType": "water", "elementType": "geometry", "stylers": [ { "color": "#ededed" }, { "lightness": 17 } ] }, 
                { "featureType": "landscape", "elementType": "geometry", "stylers": [ { "color": "#f7f7f7" }, { "lightness": 20 } ] },
                { "featureType": "road.highway", "elementType": "geometry.fill", "stylers": [ { "color": "#ffffff" }, { "lightness": 17 } ] },
                { "featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [ { "color": "#ffffff" }, { "lightness": 29 },{ "weight": 0.2 } ] }, 
                { "featureType": "road.arterial", "elementType": "geometry", "stylers": [ { "color": "#ffffff" }, { "lightness": 18 } ] },
                { "featureType": "road.local", "elementType": "geometry", "stylers": [ { "color": "#ffffff" }, { "lightness": 16 } ] }, 
                { "featureType": "poi", "elementType": "geometry", "stylers": [ { "color": "#f5f5f5" }, { "lightness": 21 } ] }, 
                { "featureType": "poi.park", "elementType": "geometry", "stylers": [ { "color": "#dedede" }, { "lightness": 21 } ] }, 
                { "elementType": "labels.text.stroke", "stylers": [ { "visibility": "on" }, { "color": "#ffffff" }, { "lightness": 16 } ] },
                { "elementType": "labels.text.fill", "stylers": [ { "saturation": 36 }, { "color": "#333333" }, { "lightness": 40 } ] },
                { "elementType": "labels.icon", "stylers": [ { "visibility": "off" } ] }, 
                { "featureType": "transit", "elementType": "geometry", "stylers": [ { "color": "#f2f2f2" }, { "lightness": 19 } ] }, 
                { "featureType": "administrative", "elementType": "geometry.fill", "stylers": [ { "color": "#fefefe" }, { "lightness": 20 } ] }, 
                { "featureType": "administrative", "elementType": "geometry.stroke", "stylers": [ { "color": "#fefefe" }, { "lightness": 17 }, { "weight": 1.2 } ] } 
            ] 
        }; 
        var mapElement = document.getElementById('destinations-map'); 
        var map = new google.maps.Map(mapElement, mapOptions); 
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {            
            map.setOptions({ 'draggable': false });
        }
        var image = 'images/map-locator.png'; 
        var markers = [];
        var bounds = new google.maps.LatLngBounds();
        <?php foreach($this->db->get('hoteles')->result() as $h): ?>
            
            markers.push(
                new google.maps.Marker({ 
                    icon: image,
                    animation: google.maps.Animation.DROP, 
                    position: new google.maps.LatLng<?= $h->ubicacion ?>, 
                    map: map, 
                    title: 'Snazzy!' 
                })
            );
            bounds.extend(new google.maps.LatLng<?= $h->ubicacion ?>);
        <?php endforeach ?>
        map.fitBounds(bounds);
    } 
   </script> <!-- youtube video player settings --> 