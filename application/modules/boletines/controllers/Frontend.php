<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();

        }                
        function ver($id = ''){
            if(is_numeric($id)){
                $detail = $this->db->get_where('boletin',array('id'=>$id))->row();
                $detail->cuerpo = $this->db->get_where('boletin_detalles',array('boletin_id'=>$detail->id));
                $view = empty($detail->contenido)?'boletin':'boletinr';
                $this->load->view('frontend/'.$view,array('detail'=>$detail));                
            }                        
        }                
    }
?>
