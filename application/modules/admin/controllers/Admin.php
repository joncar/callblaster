<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        public function ajustes(){
            $crud = $this->crud_function('','');            
            $crud->unset_add()
                     ->unset_delete()
                     ->unset_read()
                     ->unset_export();
            $action = $crud->getParameters();
            $crud = $crud->render();            
            $this->loadView($crud);
        }  
        
        function llamadas(){
            $this->as['llamadas'] = 'campanas_agenda';
            $crud = $this->crud_function('','');      
            $crud->unset_columns('orden');
            $crud->field_type('status','dropdown',array('P'=>'Pendiente','A'=>'Activa','C'=>'Completada','R'=>'Saldo insuficiente'));
            $crud->callback_column('precio',function($val){return '$'.$val; });
            $crud->callback_column('precio_venta',function($val){return '$'.$val; });
            $crud->callback_column('duracion',function($val){return ceil($val/60); });
            $crud->callback_column('je176d762.telefono',function($val,$row){return explode(' ',$row->se176d762,2)[0]; });
            $crud->callback_column('je176d762.nombre',function($val,$row){return explode(' ',$row->se176d762,2)[1]; });
            $crud->set_relation('agenda_id','agenda','{telefono} {nombre}');
            $crud->columns('je176d762.telefono','je176d762.nombre','duracion','precio','precio_venta','sid','status','fecha');
            $crud->display_as('je176d762.telefono','Teléfono')
                     ->display_as('je176d762.nombre','Nombre de contacto');
            $crud->unset_delete();            
            $crud = $crud->render();            
            $this->loadView($crud);
        }
        
        function contactos(){
            $this->as['contactos'] = 'agenda';
            $crud = $this->crud_function('','');            
            $crud->unset_delete();            
            $crud = $crud->render();            
            $this->loadView($crud);
        }
        
         function campanias(){
            $this->as['campanias'] = 'campanas';
            $crud = $this->crud_function('','');
            $crud->set_subject('Campaña');
            $crud->set_field_upload('audio','audios');
            $crud->set_relation_n_n('contactos','campanas_agenda','agenda','campanas_id','agenda_id','nombre','orden',array('user_id'=>$this->user->id));
            $crud->where('user_id',$this->user->id)
                 ->field_type('user_id','hidden',$this->user->id)
                 ->field_type('status','hidden','P')
                 ->field_type('fecha_desde','string');
            $crud->display_as('nombre_campana','Nombre de la campaña')
                 ->display_as('fecha_desde','Fecha de inicio de la campaña')
                 ->display_as('audio','Audio (.wav)')
                 ->display_as('repeticiones','Cantidad de repeticiones del audio')
                 ->display_as('mascara_numero','Numero a mostrar en la llamada');
            if($crud->getParameters()!='list'){
                $crud->display_as('mascara_numero','#Telefono del contacto (colocar numero sin guiones) Ejm: [xxx] - [xxxxxxx] - [6xxx-xxxx]');
            }
            $crud->callback_field('mascara_numero',function($val){
                $val = empty($val)?'507':$val;
                return '<input type="number" id="field-mascara_numero" name="mascara_numero" value="'.$val.'" placeholder="Numero a mostrar en la llamada" class="form-control">';
            });
            $crud->columns('nombre_campana','fecha_desde','audio','status');
            if($crud->getParameters()=='list'){
                $crud->field_type('status','dropdown',array('P'=>'Pendiente','A'=>'Activa','C'=>'Completada','R'=>'Saldo insuficiente'));
            }
            $crud->set_rules('mascara_numero','Numero a mostrar en la llamada','numeric|callback_validateNumber');
            $crud->add_action('<i class="fa fa-phone"></i> Activar Campaña','',base_url('campanas/frontend/procesar/1').'/');
            $crud = $crud->render();
            $crud->title = 'Administrar Campañas';
            $crud->crud = 'campanas';
            $this->loadView($crud);
        }
    }
?>
