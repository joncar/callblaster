<?php
require_once APPPATH.'libraries/zang/library/Zang.php';
require_once APPPATH.'/controllers/Main.php';    
class Frontend extends Main{
    protected $ssid = 'AC65889084c3b7cf19b13346978e80e40f';
    protected $auth = '0283dba102fe48838d9d0e126d82fa0b';
    
    function __construct() {
        parent::__construct();
        $this->load->model('querys');
    }                

    function procesar($dash = 0, $campanaid){        
        $zang = Zang::getInstance();
        $zang -> setOptions(array( 
            'account_sid'       => $this->ssid, 
            'auth_token'        => $this->auth,
            'response_to_array' => false
        ));
        $campana = $this->db->get_where('campanas',array('id'=>$campanaid,'status !='=>'C'));
        if($campana->num_rows()>0){
            //Integrantes
            $this->db->update('campanas',array('status'=>'A'));
            $this->db->select('campanas_agenda.id,campanas.audio, campanas.repeticiones, user.saldo, agenda.codigo_pais, agenda.telefono');
            $this->db->join('agenda','agenda.id = campanas_agenda.agenda_id');
            $this->db->join('campanas','campanas.id = campanas_agenda.campanas_id');
            $this->db->join('user','user.id = campanas.user_id');
            $integrantes = $this->db->get_where('campanas_agenda',array('campanas_id'=>$campanaid));
            if($integrantes->num_rows()>0){
                try {    
                    # NOTICE: The code bellow will initiate a new call message.
                    # Zang_Helpers::filter_e164 is a internal, wrapper helper to help you work with phone numbers and their formatting
                    # For more information about E.164, please visit: http://en.wikipedia.org/wiki/E.164
                    $precio = 0;
                    foreach($integrantes->result() as $i){                       
                        //Validar si no sobre pasa el saldo
                        $duracion = $this->querys->wavDur('audios/'.$i->audio);
                        $tipo = 'precio_'.$this->querys->getTipo($i->telefono);
                        $precio += $duracion*$i->repeticiones*$this->db->get('ajustes')->row()->$tipo;
                        $cubre = $precio<$i->saldo;
                        if($cubre){
                            $mascara = empty($campana->row()->mascara_numero)?'(507) 838-7620':$campana->row()->mascara_numero;
                            $array = array(
                                'From' => $mascara,
                                'To'   => '('.$i->codigo_pais.') '.$i->telefono,
                                'Url'  => base_url('campanas/frontend/live/'.$i->id),
                                'StatusCallback'=>base_url('campanas/frontend/getStatus/'.$i->id),
                                'IfMachine'=>$campana->row()->ifmachine
                            );
                            //print_r($array);
                            $call = $zang->create('calls',$array);
                            $this->db->update('campanas_agenda',array('sid'=>$call->sid),array('id'=>$i->id));
                            $llamada = (array)$this->db->get_where('campanas_agenda',array('id'=>$i->id))->row();
                            $llamada['campanas_agenda_id'] = $llamada['id'];
                            unset($llamada['id']);
                            unset($llamada['orden']);
                            $this->db->insert('llamadas',$llamada);
                        }else{
                            $this->db->update('campanas_agenda',array('status'=>'R'),array('id'=>$i->id));
                            $this->db->update('campanas',array('status'=>'R'),array('id'=>$campanaid));
                        }
                    }
                } catch (Zang_Exception $e) {
                    echo "Error occured: " . $e->getMessage() . "\n";
                    exit;
                }
                //Volvermos al dashboard
                if($dash==1){
                    redirect(base_url('campanas/admin/campanas/success'));
                }
            }
        }
    }

    function live($llamadaid){ 
        $this->db->update('campanas_agenda',array('status'=>'A'),array('id'=>$llamadaid));
        $this->db->update('llamadas',array('status'=>'A'),array('campanas_agenda_id'=>$llamadaid));
        if(is_numeric($llamadaid)){
            $llamada = $this->db->get_where('campanas_agenda',array('id'=>$llamadaid));
            if($llamada->num_rows()>0){
                $llamada = $llamada->row();
                $llamada->sid = trim($llamada->sid);
                $campanas = $this->db->get_where('campanas',array('id'=>$llamada->campanas_id));
                if($campanas->num_rows()>0){
                    $campanas = $campanas->row();
                    $audio = base_url('audios/'.$campanas->audio);
                    ob_end_clean();
                    header('Content-Type: text/xml');
                    $xml = "<?xml version=\"1.0\"?>\n";
                    $xml.= "<Response><Play loop='{$campanas->repeticiones}'>$audio</Play></Response>";
                    echo $xml;
                }
            }
        }
    }
    
    function getStatus($llamadaid){
        $this->db->select('campanas_agenda.*, user.id as user_id, user.saldo, agenda.codigo_pais, agenda.telefono');
        $this->db->join('agenda','agenda.id = campanas_agenda.agenda_id');
        $this->db->join('user','user.id = agenda.user_id');
        $llamada = $this->db->get_where('campanas_agenda',array('campanas_agenda.id'=>$llamadaid));
        if($llamada->num_rows()>0){
            $llamada = $llamada->row();
            $llamada->sid = trim($llamada->sid);
            $url = "https://api.zang.io/v2/Accounts/{$this->ssid}/Calls/{$llamada->sid}.json";
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($ch, CURLOPT_USERPWD, "{$this->ssid}" . ":" . "{$this->auth}");            
            $result = curl_exec($ch);
            curl_close($ch);
            $result = json_decode($result);
            $fecha = date("Y-m-d H:i:s",strtotime($result->end_time));
            //Calcular precio
            $duracion = $result->duration/60;
            $duracion = ceil($duracion);
            $telefono = $llamada->codigo_pais.$llamada->telefono;
            //Verificar si es movil o fijo
            $tipo = 'precio_'.$this->getTipo($telefono);
            $precio = $this->db->get_where('ajustes')->row()->$tipo;
            $precio = $precio*$duracion;
            //print_r($result);
            //Actualizamos los precios
            $data = array(
                'duracion'=>$result->duration,
                'precio'=>$result->price,
                'precio_venta'=>$precio,
                'status'=>$result->status=='completed'?'C':'A',
                'fecha'=>$fecha,
                'respondido_por'=>$result->answered_by
            );
            $this->db->update('campanas_agenda',$data,array('id'=>$llamadaid));
            $this->db->update('llamadas',$data,array('campanas_agenda_id'=>$llamadaid));
            //Verificar si ya todos los nmeros ee llamaron y se cierra la campaña
            if($this->db->get_where('campanas_agenda',array('campanas_id'=>$llamada->campanas_id,'status'=>'P'))->num_rows()==0){
                $this->db->update('campanas',array('status'=>'C'),array('id'=>$llamada->campanas_id));
                $this->db->update('user',array('saldo'=>$llamada->saldo-$precio),array('id'=>$llamada->user_id));
                $this->db->insert('balance',array(
                    'monto'=>$precio*-1,
                    'fecha'=>$fecha,
                    'descripcion'=>'Llamada a '.$result->to.' duracion '.$result->duration.' seg',
                    'user_id'=>$llamada->user_id,
                    'status'=>'C'
                ));
            }
        }                
    }
    
    protected function getTipo($val){       
            //Local o movil
            if(preg_match("/^58[0-9]{10}$/", $val) || preg_match("/^[0-9]{3}6[0-9]{3}[0-9]{4}$/", $val)) {
               return 'movil';
            }else{
                return 'fijo';
            }
        }
}
