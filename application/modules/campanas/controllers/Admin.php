<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function campanas(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Campaña');
            $crud->set_field_upload('audio','audios');
            $crud->set_relation_n_n('contactos','campanas_agenda','agenda','campanas_id','agenda_id','nombre','orden',array('user_id'=>$this->user->id));
            $crud->where('user_id',$this->user->id)
                 ->field_type('user_id','hidden',$this->user->id)
                 ->field_type('precio_estimado','hidden',0)
                 ->field_type('status','hidden','P')
                 ->field_type('repeticiones','hidden','1')
                 ->field_type('fecha_desde','string')
                 ->field_type('ifmachine','dropdown',array('continue'=>'Dejar el mensaje','hangup'=>'Colgar llamada'));
            $crud->display_as('nombre_campana','Nombre de la campaña')
                 ->display_as('fecha_desde','Fecha de inicio de la campaña')
                 ->display_as('audio','Audio (.wav)')
                 ->display_as('ifmachine','Que hacer si cae el buzon de mensajes?')
                 ->display_as('repeticiones','Cantidad de repeticiones del audio')
                 ->display_as('mascara_numero','Numero a mostrar en la llamada');
            if($crud->getParameters()!='list'){
                $crud->display_as('mascara_numero','#Telefono del contacto (colocar numero sin guiones) Ejm: [xxx] - [xxxxxxx] - [6xxx-xxxx]');
            }
            $crud->callback_field('mascara_numero',function($val){
                $val = empty($val)?'507':$val;
                return '<input type="number" id="field-mascara_numero" name="mascara_numero" value="'.$val.'" placeholder="Numero a mostrar en la llamada" class="form-control">';
            });
            $crud->callback_before_insert(function($post){
                $post["fecha_desde"] = date("Y-m-d H:i:s",strtotime($post["fecha_desde"]));
                return $post;
            });
            $crud->callback_column('precio_estimado',function($val,$row){                
                return '$'.get_instance()->querys->get_valor_estimado(strip_tags($row->audio),$row->repeticiones,$row->id);                
            });
            $crud->columns('nombre_campana','fecha_desde','audio','status','precio_estimado');
            if($crud->getParameters()=='list'){
                $crud->field_type('status','dropdown',array('P'=>'Pendiente','A'=>'Activa','C'=>'Completada','R'=>'Saldo insuficiente'));
            }
            $crud->set_rules('mascara_numero','Numero a mostrar en la llamada','numeric|callback_validateNumber');
            $crud->add_action('<i class="fa fa-phone"></i> Activar Campaña','',base_url('campanas/frontend/procesar/1').'/');
            $crud = $crud->render();
            $crud->title = 'Administrar Campañas';
            $crud->crud = 'campanas';
            $this->loadView($crud);
        }
        
        function validateNumber($val){       
            //Local o movil
            $this->load->model('querys');
            if(!$this->querys->is_phone($val)) {
               $this->form_validation->set_message('validateNumber','El numero de telefono introducido es incorrecto');
               return false;
            }            
        }
        
        function get_precio_estimado($audio){

        }
    }
?>
