<div class="panel panel-default">
    <div class="panel-heading">
        <h1 class="panel-title">Contactos leídos en el fichero</h1>        
    </div>
    <div class="panel-body">
        <form action="" onsubmit="return validar()" method="post">
            <table class="table table-bordered">
                <thead>
                    <tr><th>Apellidos y nombres</th><th>Teléfono</th><th>Email</th><th>Acciones</th></tr>
                </thead>
                <tbody>
                    <?php foreach($numeros as $n=>$t): ?>
                        <tr>
                            <td><?= form_input('datos['.$n.'][nombre]',$t['name'],'class="form-control name" placeholder="Nombre y Apellido"') ?></td>
                            <td><?= form_input('datos['.$n.'][telefono]',$t['phone'],'class="form-control phone" placeholder="Telefono"') ?></td>
                            <td><?= form_input('datos['.$n.'][email]',$t['email'],'class="form-control email" placeholder="Email"') ?></td>
                            <td><button class="btn btn-danger eli" style="color:red"><i class="fa fa-remove"></i> Eliminar</button></td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
            <div class="btn-group">
                <button class="btn btn-success" type="submit">Guardar contactos</button>
                <a href="<?= base_url('agenda/admin/files') ?>" class="btn btn-danger">Procesar más tarde</a>
            </div>
        </form>
    </div>
</div>
<script>
    $(document).ready(function(){
        $(".eli").click(function(){
            $(this).parents('tr').remove();
        });
    });
    
    function validar(){
        if($(".name:empty, .phone:empty").length()>0){
            alert('Verifique los numeros ya que existen campos como el nombre o el telefono vacios.');
        }else{
            return true;
        }
        return false;
    }
</script>