<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
            $this->load->model('querys');
        }
        
        function agenda(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Contacto');
            $crud->where('user_id',$this->user->id);
            $crud->field_type('user_id','hidden',$this->user->id)
                     ->field_type('codigo_pais','dropdown',array('507'=>'+507 Panama','58'=>'+58 Venezuela'));
            $crud->set_rules('telefono','Telefono','required|numeric|callback_validateNumber');
            if($crud->getParameters()!='list'){
                $crud->display_as('telefono','#Telefono del contacto (colocar numero sin guiones ni codigo de area) Ejm: [xxxxxxx] - [6xxx-xxxx]');
            }
            $crud->columns('nombre','email','telefono');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function files(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Archivo');
            $crud->where('user_id',$this->user->id);
            $crud->field_type('user_id','hidden',$this->user->id);
            $crud->columns('fichero');
            $crud->set_field_upload('fichero','files');
            $crud->add_action('<i class="fa fa-reload"></i> Procesar Fichero','',base_url('agenda/admin/importar').'/');
            $crud->set_lang_string('insert_success_message','Estamos procesando su fichero por favor espere <script>document.location.href="'.base_url('agenda/admin/importar/').'/{id}"; </script>');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function importar($id){
            if(is_numeric($id)){
                $fichero = $this->db->get_where('files',array('id'=>$id));
                if($fichero->num_rows()>0){
                    $fichero = $fichero->row();
                    if(empty($_POST)){
                        require_once 'application/libraries/Excel/SpreadsheetReader.php';
                        $excel = new SpreadsheetReader('files/'.$fichero->fichero);
                        $data = array();
                        foreach ($excel as $Row)
                        {
                            $data[] = $Row;
                        }
                        $numeros = array();
                        foreach($data as $datav){
                            $numero = array('name'=>'','phone'=>'','email'=>'');
                            foreach($datav as $value){
                                //Validamos si es un numero metemos en el array
                                if($this->querys->is_phone($value)){
                                    if(
                                        preg_match("/^[0-9]{7}$/", $value) ||
                                        preg_match("/^6[0-9]{7}$/", $value)
                                    ){
                                        $value = '507'.$value;
                                    }
                                    $numero['phone'] = $value;
                                }elseif($this->querys->is_email($value)){
                                    $numero['email'] = $value;
                                }else{
                                    $numero['name'] = !empty($value)?$value:$numero['name'];
                                }
                            }
                            $numeros[] = $numero;
                        }
                        $this->loadView(array('view'=>'panel','crud'=>'user','output'=>$this->load->view('importar',array('numeros'=>$numeros),TRUE)));
                    }else{
                        if(!empty($_POST['datos'])){
                            $datos = $_POST['datos'];                            
                            foreach($datos as $dato){
                                
                                $data = array();
                                $data['nombre'] = $dato['nombre'];
                                $data['email'] = !empty($dato['email'])?$dato['email']:'';
                                
                                //Realizamos un split del codigo del pais
                                if(substr($dato['telefono'],0,3)=='507'){
                                    $data['codigo_pais'] = '507';
                                    $dato['telefono'] = substr($dato['telefono'],3);
                                }
                                elseif(substr($dato['telefono'],0,2)=='58'){
                                    $data['codigo_pais'] = '58';
                                    $dato['telefono'] = substr($dato['telefono'],2);
                                }else{
                                 $data['codigo_pais'] = '507';                                    
                                }
                                $data['telefono'] = $dato['telefono'];
                                $data['user_id'] = $fichero->user_id;
                                if($this->db->get_where('agenda',array('telefono'=>$data['telefono'],'user_id'=>$fichero->user_id))->num_rows()>0){
                                    //Actualizamos
                                    $this->db->update('agenda',$data,array('telefono'=>$data['telefono'],'user_id'=>$fichero->user_id));
                                }else{
                                    $this->db->insert('agenda',$data);
                                }
                            }
                        }
                        redirect(base_url('agenda/admin/agenda/success'));
                    }
                }
            }
        }
        
        function validateNumber($val){       
            //Local o movil
            if(!preg_match("/^[0-9]{3}[0-9]{4}$/", $val) && !preg_match("/^6[0-9]{3}[0-9]{4}$/", $val)) {
               $this->form_validation->set_message('validateNumber','El numero de telefono introducido es incorrecto');
               return false;
            }            
        }
    }
?>
