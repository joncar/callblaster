<?php 
    require_once APPPATH.'controllers/Main.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();
        }
        
        public function paypallistener(){
            /*$_POST = array(
                "mc_gross" => 100.00,
                "protection_eligibility" => "Ineligible",
                "address_status" => "unconfirmed", 
                "payer_id" => '382NYQRKGV8ZY', 
                "tax" => 0.00, 
                "address_street" => 'Alguna Alguna',
                "payment_date" => "10:28:23 May 06, 2017 PDT", 
                "payment_status" => 'Pending',
                "charset" => 'utf-8',
                "address_zip" => 1010, 
                "first_name" => 'Jonathan', 
                "address_country_code" => 'VE', 
                "address_name" => 'Jonathan Cardozo', 
                "notify_version" => 3.8, 
                "custom" => 1, 
                "payer_status" => 'unverified', 
                "address_country" => 'Venezuela', 
                "address_city" => 'Caracas', 
                "quantity" => 1, 
                "payer_email" => 'joncar@gmail.com', 
                "verify_sign" => 'AFcWxV21C7fd0v3bYYYRCpSSRl31AVtzp-YVECiWQoBlLHM-DHUO00d2', 
                "txn_id" => '155637559R4009932', 
                "payment_type" => 'instant', 
                "last_name" => 'Cardozo', 
                "address_state" => 'Dtto Capital', 
                "receiver_email" => 'edelcidr@gmail.com', 
                "pending_reason" => 'unilateral',
                "txn_type" => 'web_accep', 
                "item_name" => 'Recargar saldo foneema', 
                "mc_currency" => 'USD', 
                "item_number" =>'', 
                "residence_country" => 'VE', 
                "test_ipn" => 1, 
                "handling_amount" => 0.00,
                "transaction_subject" =>'',
                "payment_gross" => 100.00,
                "shipping" => 0.00,
                "merchant_return_link" => 'click here', 
                "auth" => 'AEQ4X5lmAI95cJwnsyLWaAJdzuVxOja4LZ14m.t9TOqS8RxiP2Slzwgv30cgTt-zclylrSTMjjjoB4bf52ZznLg'
                );*/
            $data = array(                
                'fecha' => date("Y-m-d H:i:s"),
                'monto' => $_POST['payment_gross'],
                'status' => 'C',
                'referencia' => $_POST['txn_id'],
            );
            
            $this->db->select('user.*');
            $this->db->join('user','user.id = balance.user_id');
            $saldo = $this->db->get_where('balance',array('balance.id'=>$_POST['custom']));
            if($saldo->num_rows()>0){
                $saldo = $saldo->row();
                $this->db->update('user',array('saldo'=>$_POST['payment_gross']+$saldo->saldo),array('id'=>$saldo->id));
                $this->db->update('balance',$data,array('id'=>$_POST['custom']));
                /*$str = '<h1>Se ha recibido una recarga de saldo</h1>';
                $str.= '<p>' . $_POST['payer_email'] . ' Ha recargado saldo en chilesueldos por un valor de: ' . $_POST['payment_gross'] . '</p>';
                $str.= '<h2>Mas detalles</h2>';
                foreach ($_POST as $p => $v)
                    $str.= '<div>' . $p . ': ' . $v . '</div>';

                correo('joncar.c@gmail.com', 'Pago recibido', $str);
                correo('ignacio@chiletributa.cl', 'Pago recibido', $str);*/
            }
        }                       
    }
?>
