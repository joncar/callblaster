<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        public function balance(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Saldo');
            $crud->where('user_id',$this->user->id);
            $crud->unset_edit()
                     ->unset_delete()
                     ->unset_read()
                     ->unset_export();
            $crud->required_fields('monto');
            $crud->callback_before_insert(function($post){
                $post['descripcion'] = 'Recarga de saldo mediante Paypal';
                $post['fecha'] = date("Y-m-d H:i:s");
                $post['user_id'] = get_instance()->user->id;
                $post['status'] = 'P';
                $_SESSION['recarga'] = 1;
                return $post;
            });
            $crud->callback_column('monto',function($val){
                $color = $val<=0?'red':'blue';
                return '<div style="color:'.$color.'; text-align:right">'.$val.'</div>';
            });
            $crud->columns('referencia','fecha','descripcion','status','monto');
            $crud->field_type('status','dropdown',array('P'=>'Pendiente','A'=>'Activa','C'=>'Completada','R'=>'Saldo insuficiente'));
            $action = $crud->getParameters();
            $crud = $crud->render();
            if($action=='add'){
                $crud->output = $this->load->view('recargar',array(),TRUE);
            }else{
                $crud->crud = 'balance';
            }
            $this->loadView($crud);
        }
        
        function thank(){
            $this->loadView('thank');
        }
        
        function cancel(){
            $this->loadView('cancel');
        }
    }
?>
