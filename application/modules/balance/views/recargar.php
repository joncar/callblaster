<?= paypal_button('Recargar saldo foneema',100) ?>
<div class="container">
    <div class="col-xs-12 col-md-4 col-md-offset-4">
        <div class="well">
            <p><img src="<?= base_url('img/paypal.png') ?>" style="width:100%"></p>
            <div class="form-group">
                    <label>Indique el monto a recargar</label>
                    <input type="number" class="form-control" id='monto' value="100">
             </div>       
            <p><a href='javascript:recargar()' class="btn btn-info btn-block"><i class="fa fa-paypal"></i> Recargar Saldo Foneema</a></p>
        </div>
    </div>
</div>
<script>
    var actualizando = false;
    function recargar(){
        if(!actualizando){
            actualizando = true;
            $.post('<?= base_url('balance/admin/balance/insert') ?>',{monto:$("#monto").val()},function(data){
                actualizando = false;
                data = data.replace('<textarea>','');
                data = data.replace('</textarea>','');
                data = JSON.parse(data);
                    if(data.success){
                        $("#amount").val($("#monto").val());
                        $("#custom").val(data.insert_primary_key);
                        $("#paypalForm").submit();
                        console.log(data);
                    }else{
                        document.location.href="<?= base_url('balance/admin/cancel') ?>";
                    }
            });
        }
        else{
            alert('Por favor espere mientras procesamos su solicitud');
        }
    }
</script>