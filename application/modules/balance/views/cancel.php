<body class="no-skin">
        <?php $this->load->view('includes/header') ?>
    <div class="main-container" id="main-container">
        <?php $this->load->view('includes/sidebar') ?>
        <div class="main-content">
            <div class="main-content-inner">
                <?php $this->load->view('includes/breadcum') ?>
                <div class="page-content">
                    <div class="page-header">
                        <h1>
                            <?= empty($title) ? 'Escritorio' : $title ?>
                            <small>
                                <i class="ace-icon fa fa-angle-double-right"></i>
                            </small>
                        </h1>
                    </div><!-- /.page-header -->

                    <div class="row">
                        <div class="col-xs-12">            
                            <div class="container">
                                <div class="col-xs-12 col-md-4 col-md-offset-4">
                                    <div style="text-align: center">
                                        <i class="fa fa-remove fa-5x" style="color:red"></i> <br/> Se ha cancelado o hubo un error en el proceso de recarga, por favor vuelva a intentarlo.
                                    </div>                                    
                                    <div class="list-group">
                                            <div class="list-group-item"><b><i class="fa fa-question-circle"></i> Las posibles razones de porque esto ocurre son</b></div>
                                            <div class="list-group-item">Se ha cancelado la transacción antes de ser procesada</div>
                                            <div class="list-group-item">Hubo un error del negocio con el agente autorizado</div>
                                            <div class="list-group-item"><a href='<?= base_url('balance/admin/balance/add') ?>' class="btn btn-info btn-block"><i class="fa fa-chevron-left"></i> Volver al sistema de recarga</a></div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div>
        </div><!-- /.main-content -->			
    </div><!-- /.main-container -->
    <script src="<?= base_url("js/ace.min.js") ?>"></script>
    <script src="<?= base_url("js/jquery-ui.custom.min.js") ?>"></script>	
    <script src="<?= base_url("js/ace-elements.min.js") ?>"></script>
</body>

