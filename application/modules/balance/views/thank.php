<body class="no-skin">
        <?php $this->load->view('includes/header') ?>
    <div class="main-container" id="main-container">
        <?php $this->load->view('includes/sidebar') ?>
        <div class="main-content">
            <div class="main-content-inner">
                <?php $this->load->view('includes/breadcum') ?>
                <div class="page-content">
                    <div class="page-header">
                        <h1>
                            <?= empty($title) ? 'Escritorio' : $title ?>
                            <small>
                                <i class="ace-icon fa fa-angle-double-right"></i>
                            </small>
                        </h1>
                    </div><!-- /.page-header -->

                    <div class="row">
                        <div class="col-xs-12">            
                            <div class="container">
                                <div class="col-xs-12 col-md-4 col-md-offset-4">                                    
                                    <div style="text-align: center">
                                        <i class="fa fa-check-circle fa-5x" style="color:green"></i> <br/> Gracias por su recarga, su saldo esta en proceso de verificación.
                                    </div>
                                    <div class="list-group">            
                                            <div class="list-group-item"><b>Transacción numero: </b><?= $_POST['txn_id'] ?></div>
                                            <div class="list-group-item"><b>Monto Recargado: </b>$<?= $_POST['mc_gross'] ?></div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div>
        </div><!-- /.main-content -->			
    </div><!-- /.main-container -->
    <script src="<?= base_url("js/ace.min.js") ?>"></script>
    <script src="<?= base_url("js/jquery-ui.custom.min.js") ?>"></script>	
    <script src="<?= base_url("js/ace-elements.min.js") ?>"></script>
</body>

