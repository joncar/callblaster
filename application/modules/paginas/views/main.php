<section id="intro-7" class="intro-section">
    <?php $this->load->view('editable/main/intro-7') ?>
</section>	<!-- END INTRO-7 -->
<!-- WELCOME
============================================= -->
<section id="welcome" class="wide-50 welcome-section division editablesection">
    <?php $this->load->view('editable/main/welcome') ?>
</section>	<!-- END WELCOME -->
<!-- ABOUT
============================================= -->
<section id="nosotros" class="bg-grey wide-50 about-section division editablesection">
    <?php $this->load->view('editable/main/nosotros') ?>
</section>	 <!-- END ABOUT -->
<!-- CONTENT-1
============================================= -->
<section id="content-1" class="wide-50 content-section division editablesection">
    <?php $this->load->view('editable/main/content-1') ?>
</section>	<!-- END CONTENT-1 -->
<!-- SKILLS
============================================= -->
<section id="skills" class="bg-grey wide-50 skills-section division editablesection">
    <?php $this->load->view('editable/main/skills') ?>
</section>	<!-- END SKILLS -->
<!-- CONTENT-2
============================================= -->
<section id="content-2" class="wide-50 content-section division editablesection">
    <?php $this->load->view('editable/main/content-2') ?>
</section>	<!-- END CONTENT-2 -->	
<!-- PORTFOLIO
============================================= -->
<div id="portfolio" class="portfolio-section division editablesection">
    <?php $this->load->view('editable/main/portfolio') ?>
</div>	 <!-- END PORTFOLIO -->	
<!-- PRICING
============================================= -->
<section id="pricing" class="wide-50 pricing-section division editablesection">
    <?php $this->load->view('editable/main/pricing') ?>
</section>	<!-- END PRICING -->
<!-- CONTENT-3
============================================= -->
<section id="content-3" class="bg-grey wide-50 content-section division editablesection">
    <?php $this->load->view('editable/main/content-3') ?>
</section>	<!-- END CONTENT-3 -->
<!-- STATISTIC
============================================= -->		
<div id="statistic" class="statistic-banner division editablesection">
    <?php $this->load->view('editable/main/statistic') ?>
</div>	 <!-- END STATISTIC -->
<!-- SERVICES
============================================= -->
<section id="services" class="wide-50 services-section division editablesection">
    <?php $this->load->view('editable/main/services') ?>
</section>	<!-- END SERVICES -->
<!-- TESTIMONIALS
============================================= -->
<div id="reviews" class="reviews division editablesection">
    <?php $this->load->view('editable/main/reviews') ?>
</div>	<!-- END TESTIMONIALS -->
<!-- TEAM
============================================= -->		
<section id="team" class="wide-50 team-section division editablesection">
    <?php $this->load->view('editable/main/team') ?>
</section>
<!-- END TEAM -->

<!-- CLIENTS
============================================= -->				
<div id="clients" class="bg-grey p-top-60 p-bottom-60 clients division editablesection">
    <?php $this->load->view('editable/main/clients') ?>
</div>
<!-- END CLIENTS -->

<!-- CALL TO ACTION
============================================= -->
<section id="call-to-action" class="call-to-action division">
    <?php $this->load->view('editable/main/call-to-action') ?>
</section>	 <!-- END CALL TO ACTION -->

<!-- BLOG
============================================= -->
<section id="blog" class="bg-grey wide-50 blog-section division">
    <div class="container">
        <!-- TITLEBAR -->				
        <div class="row">
            <div class="col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2 section-title-thin">	
                <h4>From The Blog</h4>	
                <h3>Get the latest news, stories, upcoming features and offers from Altron team</h3>														
            </div>
        </div>	
        <div class="row">
            <!-- BLOG POST #1 -->
            <div class="col-md-4 m-bottom-50">
                <div class="blog-post">
                    <!-- Post Image -->
                    <div class="blog-post-img">
                        <img class="img-responsive" src="<?= base_url() ?>img/portfolio/image-5.jpg" alt="Blog Image" />	
                    </div>
                    <!-- Post Titlebar -->
                    <div class="blog-post-link">
                        <a href="#">Mauris in gravida luctus aliquam an varius tellus vitae ipsum</a>
                    </div>
                    <!-- Post Meta -->
                    <div class="blog-post-meta">
                        By <span>Jonathan Doe</span> / May 11, 2016  
                    </div>
                    <!-- Post Text -->
                    <div class="blog-post-text b-b">
                        <p>Nemo ipsam egestas volute turpis dolores aliquam quaerat sodales sapien undo euismod blandit vitae arcu. Aenean blandit non 
                            nulla at sodales mollis in eget velit ...
                        </p>
                    </div>
                </div>
            </div>	
            <!-- BLOG POST #2 -->
            <div class="col-md-4 m-bottom-50">
                <div class="blog-post">
                    <!-- Post Image -->
                    <div class="blog-post-img">
                        <img class="img-responsive" src="<?= base_url() ?>img/portfolio/image-1.jpg" alt="Blog Image" />	
                    </div>
                    <!-- Post Titlebar -->
                    <div class="blog-post-link">
                        <a href="#">Mauris in gravida luctus an varius urabitur tellus vitae ipsum</a>
                    </div>
                    <!-- Post Meta -->
                    <div class="blog-post-meta">
                        By <span>Kevin Doe</span> / May 07, 2016  
                    </div>
                   <!-- Post Text -->
                    <div class="blog-post-text b-b">
                        <p>Nemo ipsam egestas volute turpis dolores aliquam quaerat sodales sapien undo euismod blandit vitae arcu. Aenean blandit non 
                            nulla at sodales mollis in eget velit ...
                        </p>
                    </div>
                </div>
            </div>	
            <!-- BLOG POST #3 -->
            <div class="col-md-4 m-bottom-50">
                <div class="blog-post">
                    <!-- Post Image -->
                    <div class="blog-post-img">
                        <img class="img-responsive" src="<?= base_url() ?>img/portfolio/image-4.jpg" alt="Blog Image" />	
                    </div>
                    <!-- Post Titlebar -->
                    <div class="blog-post-link">
                        <a href="#">Mauris in gravida luctus aliquam an varius urabitur tellus vitae ipsum</a>
                    </div>
                    <!-- Post Meta -->
                    <div class="blog-post-meta">
                        By <span>Megan Doe</span> / April 23, 2016  
                    </div>
                    <!-- Post Text -->
                    <div class="blog-post-text b-b">
                        <p>Nemo ipsam egestas volute turpis dolores aliquam quaerat sodales sapien undo euismod blandit vitae arcu. Aenean blandit non 
                            nulla at sodales mollis in eget velit ...
                        </p>
                    </div>								
                </div>
            </div>	
        </div>	<!-- End row -->
    </div>		<!-- End container -->
</section>	<!-- END BLOG -->

<!-- CONTACTS
============================================= -->	
<section id="contacts" class="wide-100 contacts-section division">
    <div class="container">
        <div class="row">
            <!-- CONTACT TEXT -->
            <div class="col-md-6 contacts-txt">						
                <!-- Title -->
                <h3 class="h3-thin">Do you want to be part of our talanted team?</h3>
                <!-- Text -->
                <p>Feugiat eros, ac tincidunt ligula massa in est. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices cubilia curae. 
                    Integer congue leo metus, eu mollis lorem viverra magna ipsum, lobortis imperdiet lacus, at ultricies nisi porttitor luctus
                </p>
                <!-- Contact Data -->
                <div class="contacts-info">
                    <!-- Address -->
                    <div class="contact-info-item m-top-20">
                        <h4>Our Location</h4>									
                        <p>PO Box 16122 Collins Street West Victoria 8007 Australia</p>
                        <p>121 King St, Melbourne VIC 3000, Australia</p>
                    </div>
                    <!-- Phones -->
                    <div class="contact-info-item m-top-20">
                        <h4>Contact Information</h4>
                        <p> Phone : +01 2 3456 7890</p>
                        <p> Fax : +09 8 7654 3210</p>
                        <p> Email: <span>hello@yourdomain.com</span></p>
                    </div>
                </div>	
            </div>
            <!-- CONTACT FORM -->
            <div class="col-md-6 contacts-form white-color">
                <!-- Title -->
                <h3 class="h3-thin">Don’t hesitate to contact us</h3>
                <!-- Text -->
                <p class="m-bottom-20">Cursus risus laoreet auctor, pharetra massa varius dignissim sollicidin aliquam.Nulla dolor sapien, fringilla risus nec</p>
                <!-- Form -->
                <form id="contact-form" name="contactform" class="row">
                    <!-- Contact Form Input -->
                    <div id="input_name" class="col-lg-6">
                        <input type="text" name="name" id="name" class="form-control" placeholder="Name"> 
                    </div>
                    <div id="input_email" class="col-lg-6">
                        <input type="text" name="email" id="email" class="form-control" placeholder="Email"> 
                    </div>
                    <div id="input_subject" class="col-md-12">
                        <input type="text" name="subject" id="subject" class="form-control" placeholder="Subject"> 
                    </div>
                    <div id="input_message" class="col-md-12">
                        <textarea class="form-control" name="message" id="message" rows="6" placeholder="Your Message ..."></textarea>
                    </div> 
                    <!-- Contact Form Button -->
                    <div id="form_btn" class="col-md-12 m-top-10">	

                        <input type="submit" value="Send Your Message" id="submit" class="btn btn-lg">		
                        <p class="m-top-20">*By signing up, you agree to our <a href="#">Terms of Service</a> and <a href="#">Privacy Policy</a></p>

                    </div>
                    <!-- Contact Form Message -->
                    <div class="col-md-12 contact-form-msg">
                        <span class="loading"></span>
                    </div>	
                </form>													
            </div>
        </div>		<!-- End row -->
    </div>	   <!-- End container -->
</section>	<!-- END CONTACTS -->	
