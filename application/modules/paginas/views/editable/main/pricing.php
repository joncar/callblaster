<div class="container">
        <!-- TITLEBAR -->				
        <div class="row">
            <div class="col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2 section-title-thin">		
                <h4>Packages and Pricing</h4>	
                <h3>Select the pricing plan that suits your needs. Upgrade, downgrade or cancel anytime</h3>														
            </div>
        </div>	
        <!-- PRICING TABLES HOLDER -->
        <div class="row pricing-row">
            <!-- PRICE PLAN STANDARD -->
            <div class="col-sm-4 col-md-4 m-bottom-50">
                <div class="pricing-table text-center">		
                    <!-- Table Header  -->
                    <h3>Standard</h3>
                    <!-- Plan Price  -->
                    <div class="price">
                        <sup>$</sup>
                        <span>39</span>	
                        <p>Per Month</p>
                    </div>
                    <!-- Plan Features  -->
                    <ul class="features">
                        <li>Up to10 Projects</li>
                        <li>10 GB of Storage</li>
                        <li>Up to 250 Users</li>
                        <li>10 mySQL Database</li>
                    </ul>
                   <!-- Table Button  -->
                    <a href="#" class="btn btn-lg">Select Plan</a>
                </div>
            </div>
            <!-- PRICE PLAN ADVANCED -->
            <div class="col-sm-4 col-md-4 m-bottom-50">	
                <div class="pricing-table text-center highlight">		
                    <!-- Table Header  -->
                    <h3>Advanced</h3>
                    <div class="down-arrow"></div>
                    <!-- Plan Price  -->
                    <div class="price">
                        <sup>$</sup>
                        <span>69</span>	
                        <p>Per Month</p>
                    </div>
                    <!-- Plan Features  -->
                    <ul class="features">
                        <li>Unlimited Projects</li>
                        <li>50 GB of Storage</li>
                        <li>Up to 1000 Users</li>
                        <li>10 mySQL Database</li>
                    </ul>
                   <!-- Table Button  -->
                    <a href="#" class="btn btn-lg">Select Plan</a>
                </div>
            </div>
            <!-- PRICE PLAN ULTIMATE -->
            <div class="col-sm-4 col-md-4 m-bottom-50">
                <div class="pricing-table text-center">		
                    <!-- Table Header  -->
                    <h3>Ultimate</h3>
                    <!-- Plan Price  -->
                    <div class="price">
                        <sup>$</sup>
                        <span>99</span>	
                        <p>Per Month</p>
                    </div>
                    <!-- Plan Features  -->
                    <ul class="features">
                        <li>Unlimited Projects</li>
                        <li>150 GB of Storage</li>
                        <li>Unlimited Users</li>
                        <li>10 mySQL Database</li>
                    </ul>
                   <!-- Table Button  -->
                    <a href="#" class="btn btn-lg">Select Plan</a>
                </div>
            </div>
        </div>	 <!-- END PRICING TABLES HOLDER -->
        <!-- PRICING NOTICE MESSAGE  -->
        <div class="row pricing-notice">				 	
            <div class="col-md-12 text-center m-bottom-50">					
                <p>*To explore <span>your best plan</span> or for a custom quote, please contact sales at <span>(985) 765-4321</span> </p>							
            </div>							
        </div>
    </div>    <!-- End container -->