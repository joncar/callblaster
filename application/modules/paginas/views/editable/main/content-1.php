<div class="container">
    <div class="row">
        <!-- CONTENT TEXT -->
        <div class="col-md-6 col-md-offset-6 content-1-txt p-left-60 m-bottom-50">
            <!-- Title -->
            <h3 class="h3-thin">We work hard for your. We have perfect solutions for all your needs!</h3>
            <!-- Text -->
            <p>Quisque magna ipsum, lobortis nulla consequat, consequat tempus massa. Mauris placerat iaculis nisl vestibulum vestibulum 
                elit tincidunt euismod purus lobortis. Bibendum non diam tempus aliquet. Mauris porta imperdiet lacus, at ultricies nisi 
                porttitor luctus. Phasellus placerat
            </p>
            <!-- List Item #1 -->
            <div class="cbox-1 m-top-20">
                <!-- Icon -->
                <div class="cbox-1-icon"><i class="fa fa-check" aria-hidden="true"></i></div>
                <!-- Text -->
                <div class="cbox-1-txt">
                    <p>Porta, feugiat primis in luctus vehicula pharetra ultrice</p>
                </div>
            </div>
            <!-- List Item #2 -->
            <div class="cbox-1">
                <!-- Icon -->
                <div class="cbox-1-icon"><i class="fa fa-check" aria-hidden="true"></i></div>
                <!-- Text -->
                <div class="cbox-1-txt">
                    <p>Quisque magna ipsum, lobortis imperdiet consequat, mollis risus tempus massa semper gravida</p>
                </div>
            </div>	
            <!-- List Item #3 -->
            <div class="cbox-1">
                <!-- Icon -->
                <div class="cbox-1-icon"><i class="fa fa-check" aria-hidden="true"></i></div>
                <!-- Text -->
                <div class="cbox-1-txt">
                    <p>Praesent semper, lacus cursus porta,feugiat primis in luctus vehicula pharetra ultrice</p>
                </div>
            </div>
            <!-- List Item #4 -->
            <div class="cbox-1">
                <!-- Icon -->
                <div class="cbox-1-icon"><i class="fa fa-check" aria-hidden="true"></i></div>
                <!-- Text -->
                <div class="cbox-1-txt">
                    <p>Porta,feugiat primis in luctus vehicula pharetra ultrice</p>
                </div>
            </div>
            <!-- List Item #5 -->
            <div class="cbox-1">
                <!-- Icon -->
                <div class="cbox-1-icon"><i class="fa fa-check" aria-hidden="true"></i></div>
                <!-- Text -->
                <div class="cbox-1-txt">
                    <p>Quisque magna ipsum, lobortis imperdiet consequat, mollis risus tempus massa semper gravida</p>
                </div>
            </div>
            <!-- Button -->
            <a href="#" class="btn btn-lg m-top-20">Discover More</a>
        </div>
    </div>	<!-- End row -->				
</div>	 <!-- End container -->
<!-- CONTENT-1 IMAGE -->
<div class="content-1-img"></div>