<div class="container">
        <div class="row">
            <!-- TESTIMONIALS ROTATOR CONTENT -->
            <div class="col-md-6 p-right-60">							
                <div class="flexslider white-color">											
                    <ul class="slides">
                        <!-- TESTIMONIAL #1 -->
                        <li>
                            <div class="review text-center m-bottom-50">
                                <!-- Testimonial Text -->
                                <div class="testimonial-txt m-bottom-20 white-color">
                                    <p>Etiam sapien sem accumsan at sagittis elementum congue augue egestas volutpat vulputate fermentum suscipit egestas lobortis magna 
                                        suscipit egestas lobortis magna, fermentum mauris interdum								   
                                    </p>												
                                </div>
                                <!-- Testimonial Author -->
                                <p class="author">&ndash; Jonathan Doe</p>
                                <span>Programmer</span>
                            </div>
                        </li>
                        <!-- TESTIMONIAL #2 -->	
                        <li>
                            <div class="review text-center m-bottom-50">
                                <!-- Testimonial Text -->
                                <div class="testimonial-txt m-bottom-20 white-color">
                                    <p>Cursus risus laoreet auctor, pharetra massa varius dignissim sollicidin aliquam.Nulla dolor sapien, fringilla risus nec, luctus 
                                        porttitor mauris donec diam euismod ultrice sapien egestas magna
                                    </p>												
                                </div>
                                <!-- Testimonial Author -->
                                <p class="author">&ndash; Karen Doe</p>
                                <span>Housewife</span>
                            </div>
                        </li>
                        <!-- TESTIMONIAL #3 -->
                        <li>
                            <div class="review text-center m-bottom-50">
                                <!-- Testimonial Text -->
                                <div class="testimonial-txt m-bottom-20 white-color">
                                    <p>Dapibus egestas lobortis magna phasellus nec euismod mauris rutrum turpis. Ratione voluptatem sequi nesciunt, neque porro quisquam 
                                        dolorem ipsum, tempora incidunt molestie posuere
                                    </p>													
                                </div>	
                                <!-- Testimonial Author -->
                                <p class="author">&ndash; Jonathan Doe</p>
                                <span>SEO Manager</span>
                            </div>
                        </li>
                        <!-- TESTIMONIAL #4 -->
                        <li>
                            <div class="review text-center m-bottom-50">
                                <!-- Testimonial Text -->
                                <div class="testimonial-txt m-bottom-20 white-color">
                                    <p>Vivamus a purus lacus tempor egestas lobortis magna volutpat. Etiam placerat rutrum aliquet. Aliquam a augue suscipit, bibendum luctus neque. 
                                        Vestibulum ipsum, tellus mauris donec euismod
                                    </p>												
                                </div>
                                <!-- Testimonial Author -->
                                <p class="author">&ndash; Hannah Doe</p>
                                <span>Internet Surfer</span>
                            </div>
                        </li>
                    </ul>												
                </div>				
            </div>		<!-- END TESTIMONIALS ROTATOR CONTENT -->
        </div>	 <!-- End row -->					
    </div>	 <!-- End container -->	
    <!-- TESTIMONIALS IMAGE -->
    <div class="reviews-img"></div>