<div class="overlay division">
        <div class="container">		
            <div id="intro-7-content" class="row">
                <!-- INTRO TEXT -->
                <div class="col-md-10 col-md-offset-1 intro-txt text-center white-color">
                    <!-- Title -->
                    <h2>Promociona tus productos con llamadas automatizadas</h2>	
                    <!-- Button -->
                    <a class="btn btn-lg btn-tra m-top-10" href="#">Más información</a>		
                </div>
            </div>	 <!-- End Intro Content -->
        </div>    <!-- End container -->
    </div>	   <!-- End overlay -->