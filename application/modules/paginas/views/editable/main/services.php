<div class="container">
       <!-- TITLEBAR -->				
        <div class="row">
            <div class="col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2 section-title-thin">	
                <h4>Our Core Services</h4>	
                <h3>Altron includes many element variations to showcase your content.</h3>														
            </div>
        </div>	
        <!-- SERVICES BOXES -->
        <div class="row">						
            <ul>
                <!-- SERVICE BOX #1 -->
                <li class="col-md-6 sbox m-bottom-50"> 
                    <!-- Icon -->
                    <span class="ti-desktop"></span>
                    <!-- Text -->
                    <div class="sbox-txt">
                        <h4 class="h4-lg">Fully Responsive Layout</h4>
                        <p>Etiam sapien sem accumsan at sagittis elementum congue augue egestas volutpat vulputate fermentum suscipit egestas lobortis magna 
                            suscipit egestas lobortis magna, fermentum mauris interdum lectus										   
                        </p>
                    </div>
                </li>
                <!-- SERVICE BOX #2 -->
                <li class="col-md-6 sbox m-bottom-50"> 
                    <!-- Icon -->
                    <span class="ti-eraser"></span>
                    <!-- Text -->
                    <div class="sbox-txt">
                        <h4 class="h4-lg">Ultra Modern Design</h4>
                        <p>Etiam sapien sem accumsan at sagittis elementum congue augue egestas volutpat vulputate fermentum suscipit egestas lobortis magna 
                            suscipit egestas lobortis magna, fermentum mauris interdum lectus										   
                        </p>
                    </div>
                </li> 
                <!-- SERVICE BOX #3 -->
                <li class="col-md-6 sbox m-bottom-50"> 
                    <!-- Icon -->
                    <span class="ti-ruler-pencil"></span>
                    <!-- Text -->
                    <div class="sbox-txt">
                        <h4 class="h4-lg">Extremely Flexible</h4>
                        <p>Etiam sapien sem accumsan at sagittis elementum congue augue egestas volutpat vulputate fermentum suscipit egestas lobortis magna 
                            suscipit egestas lobortis magna, fermentum mauris interdum lectus										   
                        </p>
                    </div>
                </li>	
                <!-- SERVICE BOX #4 -->
                <li class="col-md-6 sbox m-bottom-50"> 
                    <!-- Icon -->
                    <span class="ti-world"></span>
                    <!-- Text -->
                    <div class="sbox-txt">
                        <h4 class="h4-lg">SEO Optimized</h4>
                        <p>Etiam sapien sem accumsan at sagittis elementum congue augue egestas volutpat vulputate fermentum suscipit egestas lobortis magna 
                            suscipit egestas lobortis magna, fermentum mauris interdum lectus										   
                        </p>
                    </div>
                </li>
                <!-- SERVICE BOX #5 -->
                <li class="col-md-6 sbox m-bottom-50"> 
                    <!-- Icon -->
                    <span class="ti-user"></span>
                    <!-- Text -->
                    <div class="sbox-txt">
                        <h4 class="h4-lg">Social Media</h4>
                        <p>Etiam sapien sem accumsan at sagittis elementum congue augue egestas volutpat vulputate fermentum suscipit egestas lobortis magna 
                            suscipit egestas lobortis magna, fermentum mauris interdum lectus										   
                        </p>
                    </div>
                </li>
                <!-- SERVICE BOX #6 -->
                <li class="col-md-6 sbox m-bottom-50"> 
                    <!-- Icon -->
                    <span class="ti-comment-alt"></span>
                    <!-- Text -->
                    <div class="sbox-txt">
                        <h4 class="h4-lg">24/7 Free Support </h4>
                        <p>Etiam sapien sem accumsan at sagittis elementum congue augue egestas volutpat vulputate fermentum suscipit egestas lobortis magna 
                            suscipit egestas lobortis magna, fermentum mauris interdum lectus										   
                        </p>
                    </div>
                </li> 
            </ul>																									
        </div>	  <!-- End row -->					
    </div>	   <!-- End container -->	