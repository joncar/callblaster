<div class="container">	
    <div class="row">
        <!-- SKILLS TEXT --> 
        <div class="col-md-7 col-lg-6 skills-txt m-bottom-50">
            <!-- Title-->													
            <h4 class="h4-lg">Web Design and UI / UX </h4>
            <!-- Text -->
            <p> Duis bibendum eros eu libero tempus aliquam. Donec hendrerit, est in eleifend gravida, enim magna imperdiet est,
                eros ut lorem. In purus dui, lacinia id efficitur sit amet, bibendum quis ligula ligula laoreet viverra, tortor							  
            </p>
            <!-- Title-->	
            <h4 class="h4-lg m-top-20">Corporate Style and Branding</h4>
            <!-- Text -->
            <p>In purus dui, lacinia id efficitur sit amet, bibendum quis ligula ligula laoreet viverra, tortor
                Nullam rutrum blandit convallis. Maecenas mauris in gravida, aliquam varius fringilla imperdiet. Nemo ipsam egestas volute
            </p>
        </div>
        <!-- SKILLS HOLDER --> 
        <div class="col-md-5 col-lg-6 m-bottom-50 p-left-45">
            <!-- Design --> 
            <div class="barWrapper">
                <h5>Design</h5>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100" >   
                        <span class="popOver" data-toggle="tooltip" data-placement="top" title="95%"> </span>     
                    </div>
                </div>
            </div>
            <!-- Photography  --> 
            <div class="barWrapper">
                <h5>Photography </h5>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="92" aria-valuemin="0" aria-valuemax="100" >   
                        <span class="popOver" data-toggle="tooltip" data-placement="top" title="92%"> </span>     
                    </div>
                </div>
            </div>
            <!-- Web Development  --> 
            <div class="barWrapper">
                <h5>Web Development </h5>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="87" aria-valuemin="0" aria-valuemax="100" >   
                        <span class="popOver" data-toggle="tooltip" data-placement="top" title="87%"> </span>     
                    </div>
                </div>
            </div>
            <!-- Marketing  --> 
            <div class="barWrapper">
                <h5>Marketing </h5>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" >   
                        <span class="popOver" data-toggle="tooltip" data-placement="top" title="90%"> </span>     
                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- End row -->
</div>	   <!-- End container -->