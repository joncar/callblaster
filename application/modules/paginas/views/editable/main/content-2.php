<div class="container">	
    <div class="row">
        <!-- CONTENT TEXT -->
        <div class="col-md-6 content-2-txt p-right-30 m-bottom-50">
            <!-- CONTENT BOX #1 -->
            <div class="cbox-2 m-bottom-30">							
                <span class="ti-pencil-alt"></span>
                <h4 class="h4-lg">Comment every section</h4>

                <p>Duis efficitur imperdiet pulvinar phasele imperdiet pulvinare etiam nibh, venenatis ac massa accumsan dolores
                    ipsum vitae purus scelerisque efficitur ipsum primis in posuere cubilia laoreet augue egestas
                </p>								
            </div>
            <!-- CONTENT BOX #2 -->
            <div class="cbox-2 m-bottom-30">							
                <span class="ti-layout"></span>
                <h4 class="h4-lg">Well organized code</h4>

                <p>Duis efficitur imperdiet pulvinar phasele imperdiet pulvinare etiam nibh, venenatis ac massa accumsan dolores eos
                    ratione mattis a semper non, volutpat nullam et fringilla vitae pulvinar
                </p>								
            </div>
            <!-- CONTENT BOX #3 -->
            <div class="cbox-2 m-bottom-30">							
                <span class="ti-book"></span>
                <h4 class="h4-lg">Detailed help file</h4>

                <p> Pellentesque sapien purus, sagittis eu accumsan convallis, vehicula ut lectus. Fusce accumsan purus pretium ligula vehicula, ut interdum nisl vulputate.
                    Vivamus ultrices luctus quam eu feugiat leifend gravida
                </p>							
            </div>
            <!-- CONTENT BOX #4 -->
            <div class="cbox-2 m-bottom-30">							
                <span class="ti-support"></span>
                <h4 class="h4-lg">Premium free support</h4>
                <p>Pellentesque sapien purus, sagittis eu accumsan convallis, vehicula ut lectus. Fusce accumsan purus pretium ligula vehicula, ut interdum nisl vulputate.
                    Vivamus ultrices luctus quam eu feugiat leifend gravida
                </p>							
            </div>
        </div>
    </div>	  <!-- End row -->
</div>	   <!-- End container -->
<!-- CONTENT-2 IMAGE -->
<div class="content-2-img"></div>