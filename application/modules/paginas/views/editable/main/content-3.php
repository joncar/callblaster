<div class="container">	
        <div class="row">
            <!-- CONTENT TEXT -->
            <div class="col-md-6 content-3-txt m-top-30 p-right-45 m-bottom-50">
                <!-- CONTENT BOX #1 -->
                <div class="cbox-3">
                    <!-- Title -->
                    <h3 class="h3-thin">A wonderful & cool landing page </h3>
                    <!-- Text -->
                    <p>Pellentesque sapien purus, sagittis eu accumsan convallis, vehicula lectus. Fusce accumsan purus pretium ligula vehicula, 
                        ut interdum vivamus ultrices luctus quam  primis in faucibus orci luctus ultrices posuere cubilia
                    </p>	
                </div>
                <!-- CONTENT BOX #2 -->
                <div class="cbox-3 m-top-30 m-bottom-30">
                    <!-- Title -->
                    <h3 class="h3-thin">The best website for your project</h3>
                    <!-- Text -->														
                    <p>Feugiat eros, ac tincidunt ligula massa in est. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; 
                        Integer congue leo metus, eu mollis lorem viverra nec. Duis efficitur imperdiet pulvinar phasele imperdiet pulvinare etiam nibh, venenatis ac massa 
                    </p>
                </div>							
                <!-- Button -->
                <a class="btn btn-lg" href="#">Get More Information</a>	
            </div>
            <!-- CONTENT IMAGE -->
            <div class="col-md-6 content-3-img text-center m-bottom-50">
                <img class="img-responsive" src="<?= base_url() ?>img/thumbs/content-3.png" alt="content-image">
            </div>
        </div>	  <!-- End row -->
    </div>	   <!-- End container -->