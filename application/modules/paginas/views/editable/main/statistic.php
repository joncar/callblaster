<div class="container">
        <div class="row">
            <!-- STATISTIC BLOCK #1 -->
            <div class="col-sm-3 statistic-block text-center m-bottom-50">							
                <div class="statistic-number">7656</div>
                <p>Lines of Code</p>						
            </div>
            <!-- STATISTIC BLOCK #2 -->
            <div class="col-sm-3 statistic-block text-center m-bottom-50">		
                <div class="statistic-number">443</div>
                <p>Satisfied Clients</p>							
            </div>
            <!-- STATISTIC BLOCK #3 -->
            <div class="col-sm-3 statistic-block text-center m-bottom-50">		
                <div class="statistic-number">245</div>
                <p>Cups of Coffee</p>							
            </div>
            <!-- STATISTIC BLOCK #4 -->
            <div class="col-sm-3 statistic-block text-center m-bottom-50">		
                <div class="statistic-number">845</div>
                <p>Sleeping Hours </p>						
            </div>
        </div>	 <!-- End row -->
    </div>	 <!-- End container -->