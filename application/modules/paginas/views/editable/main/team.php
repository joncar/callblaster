<div class="container">	
        <!-- TITLEBAR -->				
        <div class="row">
            <div class="col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2 section-title-thin">		
                <h4>Our Creative Team</h4>	
                <h3>Every member of our team has spent many hours polishing professional skills</h3>														
            </div>
        </div>	
        <!-- TEAM MEMBERS HOLDER -->
        <div class="row team_members_wrapper">
            <!-- TEAM MEMBER #1 -->
            <div class="col-sm-6 col-md-3 text-center m-bottom-50">
                <div class="team-member">
                    <!-- Team Member Photo  -->	
                    <div class="img-block">
                        <img class="img-responsive" src="<?= base_url() ?>img/thumbs/team-1.jpg" alt="team_foto" >
                    </div>
                    <!-- Team Member Meta  -->		
                    <div class="team-img-meta">
                        <h4 class="h4-lg">Jonathan Doe</h4>
                        <h6>Founder</h6>
                        <p>Suscipit imperdiet sceleris integer posuere erat primis in faucibus</p>	
                    </div>
                    <!-- Team Member Social Icons  -->		
                    <div class="team-socials">																	
                        <ul class="team-member-socials clearfix">
                            <li><a class="page_social ico_facebook" href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a class="page_social ico_twitter" href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a class="page_social ico_pinterest" href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>											
                            <li><a class="page_social ico_skype" href="#"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>	
                </div>
            </div>	<!-- END TEAM MEMBER #1 -->
            <!-- TEAM MEMBER #2 -->
            <div class="col-sm-6 col-md-3 text-center m-bottom-50">				
                <div class="team-member">
                    <!-- Team Member Photo  -->	
                    <div class="img-block">
                        <img class="img-responsive" src="<?= base_url() ?>img/thumbs/team-2.jpg" alt="team_foto" >
                    </div>
                    <!-- Team Member Meta  -->		
                    <div class="team-img-meta">
                        <h4 class="h4-lg">Hannah Doe</h4>
                        <h6>IT Consultant</h6>	
                        <p>Suscipit imperdiet sceleris integer posuere erat primis in faucibus</p>	
                    </div>
                    <!-- Team Member Social Icons  -->		
                    <div class="team-socials">																	
                        <ul class="team-member-socials clearfix">
                            <li><a class="page_social ico_facebook" href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a class="page_social ico_twitter" href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a class="page_social ico_tumblr" href="#"><i class="fa fa-tumblr" aria-hidden="true"></i></a></li>	
                            <li><a class="page_social ico_behance" href="#"><i class="fa fa-behance" aria-hidden="true"></i></a></li>									
                            <li><a class="page_social ico_deviantart" href="#"><i class="fa fa-deviantart" aria-hidden="true"></i></a></li>														
                        </ul>
                    </div>	
                </div>
            </div>	<!-- END TEAM MEMBER #2 -->					
            <!-- TEAM MEMBER #3 -->
            <div class="col-sm-6 col-md-3 text-center m-bottom-50">
                <div class="team-member">
                    <!-- Team Member Photo  -->	
                    <div class="img-block">
                        <img class="img-responsive" src="<?= base_url() ?>img/thumbs/team-3.jpg" alt="team_foto" >
                    </div>
                    <!-- Team Member Meta  -->		
                    <div class="team-img-meta">
                        <h4 class="h4-lg">Robert Doe</h4>
                        <h6>Designer</h6>	
                        <p>Suscipit imperdiet sceleris integer posuere erat primis in faucibus</p>	
                    </div>
                    <!-- Team Member Social Icons  -->		
                    <div class="team-socials">																	
                        <ul class="team-member-socials clearfix">
                            <li><a class="page_social ico_facebook" href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a class="page_social ico_twitter" href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a class="page_social ico_youtube" href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                            <li><a class="page_social ico_tumblr" href="#"><i class="fa fa-tumblr" aria-hidden="true"></i></a></li>																
                        </ul>
                    </div>	
                </div>
            </div>	<!-- END TEAM MEMBER #3 -->
            <!-- TEAM MEMBER #4 -->
            <div class="col-sm-6 col-md-3 text-center m-bottom-50">
                <div class="team-member">
                    <!-- Team Member Photo  -->	
                    <div class="img-block">
                        <img class="img-responsive" src="<?= base_url() ?>img/thumbs/team-4.jpg" alt="team_foto" >
                    </div>
                    <!-- Team Member Meta  -->		
                    <div class="team-img-meta">
                        <h4 class="h4-lg">Megan Doe</h4>
                        <h6>Support</h6>
                        <p>Suscipit imperdiet sceleris integer posuere erat primis in faucibus</p>	
                    </div>
                    <!-- Team Member Social Icons  -->		
                    <div class="team-socials">																	
                        <ul class="team-member-socials clearfix">
                            <li><a class="page_social ico_facebook" href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a class="page_social ico_twitter" href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a class="page_social ico_instagram" href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            <li><a class="page_social ico_google-plus" href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>														
                        </ul>
                    </div>	
                </div>
            </div>	<!-- END TEAM MEMBER #4 -->
        </div>	<!-- END TEAM MEMBERS HOLDER -->
    </div>	   <!-- End container -->