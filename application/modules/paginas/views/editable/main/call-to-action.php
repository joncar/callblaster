<div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 text-center white-color">
                <!-- Title -->
                <h2>Powerfully Creativity <span>with</span> Clean Code and Ultra <span>Modern Design</span></h2>
                <!-- Button -->
                <div class="cta-buttons">
                    <a href="#" class="btn btn-lg">Purchase Now</a>
                    <a href="#" class="btn btn-lg btn-tra">See More First</a>
                </div>
            </div>
        </div>	   <!-- End row -->
    </div>		<!-- End container -->