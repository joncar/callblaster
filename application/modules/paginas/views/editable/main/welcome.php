<div class="container">		
        <div class="row">
            <!-- WELCOME QUOTE -->
            <div class="col-lg-4 welcome-quote text-right m-bottom-50">
               <!-- Quote -->
                <h3>"We believe in the power of simple, effective design, and believe that power should be wielded by all"</h3>							
                <p>Words by William Doe</p>
            </div>
            <!-- WELCOME TEXT -->
            <div class="col-md-6 col-lg-4 welcome-txt m-bottom-50">
                <!-- Text -->
               <div class="p-bottom-20">
                    <p>Quisque magna ipsum, lobortis nulla consequat, consequat tempus massa. Mauris placerat iaculis nisl vestibulum vestibulum 
                        elit tincidunt euismod purus lobortis. Bibendum non diam tempus aliquet. Mauris porta imperdiet lacus, at ultricies nisi 
                        porttitor luctus. Phasellus placerat massa nec faucibus tempus dolor, felis orci lacinia risus
                    </p>
                    <p>Etiam scelerisque semper justo ut convallis. Cras tempor, non convallis tincidunt, est eros tincidunt ligula, 
                        dictum magna eu ligula, lacus eros varius fringilla imperdiet sodales sapien porttitor mauris 
                    </p>
                </div>
                <!-- Slogan -->
                <div class="slogan-txt b-top p-top-20 p-bottom-20">
                    <h4 class="h4-lg">We're team of developers and designers. We love to design and develop ultra modern templates</h4>							
                </div>
            </div>
            <!-- WELCOME IMAGE -->
            <div class="col-md-6 col-lg-4 welcome-img m-bottom-50">
                <img class="img-responsive" src="<?= base_url() ?>img/thumbs/welcome.jpg" alt="welcome-image">														
            </div>
        </div>	  <!-- End row -->  
    </div>	   <!-- End container -->