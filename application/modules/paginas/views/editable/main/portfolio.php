<!-- PORTFOLIO IMAGES HOLDER -->	
    <div class="row portfolio-items-list">							
        <ul>
            <!-- IMAGE #1 -->
            <li class="col-sm-6 col-md-3 portfolio-item">
                <div class="hover-overlay"> 
                   <!-- Image Link -->
                    <img class="img-responsive" src="<?= base_url() ?>img/portfolio/image-1.jpg" alt="Portfolio Image">
                    <!-- Image Zoom -->
                    <a class="prettyPhoto image_zoom" href="<?= base_url() ?>img/portfolio/image-1.jpg" title="Portfolio Image">	
                        <!-- Overlay Content -->
                        <div class="item-overlay">											
                            <div class="overlay-content">
                                <h4>Enter Title Here</h4>
                                <p>Aliquam a augue suscipit, bibendum luctus neque laoreet rhoncus tellus vulputate congue</p>
                            </div>												
                        </div>
                    </a>	<!-- End Image Zoom -->
               </div>	<!-- End Hover Overlay -->
            </li>
            <!-- IMAGE #2 -->
            <li class="col-sm-6 col-md-3 portfolio-item">
                <div class="hover-overlay"> 
                    <!-- Image Link -->
                    <img class="img-responsive" src="<?= base_url() ?>img/portfolio/image-2.jpg" alt="Portfolio Image">
                    <!-- Image Zoom -->
                    <a class="prettyPhoto image_zoom" href="<?= base_url() ?>img/portfolio/image-2.jpg" title="Portfolio Image">	
                        <!-- Overlay Content -->
                        <div class="item-overlay">											
                            <div class="overlay-content">
                                <h4>Enter Title Here</h4>
                                <p>Aliquam a augue suscipit, bibendum luctus neque laoreet rhoncus tellus vulputate congue</p>
                            </div>												
                        </div>	
                    </a>	<!-- End Image Zoom -->
                </div>	<!-- End Hover Overlay -->
            </li>
            <!-- IMAGE #3 -->
            <li class="col-sm-6 col-md-3 portfolio-item">
                <div class="hover-overlay"> 
                    <!-- Image Link -->
                    <img class="img-responsive" src="<?= base_url() ?>img/portfolio/image-3.jpg" alt="Portfolio Image">
                   <!-- Image Zoom -->
                    <a class="prettyPhoto image_zoom" href="<?= base_url() ?>img/portfolio/image-3.jpg" title="Portfolio Image">	
                        <!-- Overlay Content -->
                        <div class="item-overlay">											
                            <div class="overlay-content">
                                <h4>Enter Title Here</h4>
                                <p>Aliquam a augue suscipit, bibendum luctus neque laoreet rhoncus tellus vulputate congue</p>
                            </div>												
                        </div>	
                    </a>	<!-- End Image Zoom -->
                </div>	<!-- End Hover Overlay -->
            </li>
            <!-- IMAGE #4 -->
            <li class="col-sm-6 col-md-3 portfolio-item">
                <div class="hover-overlay"> 
                    <!-- Image Link -->
                    <img class="img-responsive" src="<?= base_url() ?>img/portfolio/image-4.jpg" alt="Portfolio Image">
                    <!-- Image Zoom -->
                    <a class="prettyPhoto image_zoom" href="<?= base_url() ?>img/portfolio/image-4.jpg" title="Portfolio Image">	
                        <!-- Overlay Content -->
                        <div class="item-overlay">											
                            <div class="overlay-content">
                                <h4>Enter Title Here</h4>
                                <p>Aliquam a augue suscipit, bibendum luctus neque laoreet rhoncus tellus vulputate congue</p>
                            </div>												
                        </div>
                    </a>	<!-- End Image Zoom -->
                </div>	<!-- End Hover Overlay -->
            </li>
            <!-- IMAGE #5 -->
            <li class="col-sm-6 col-md-3 portfolio-item">
                <div class="hover-overlay"> 
                    <!-- Image Link -->
                    <img class="img-responsive" src="<?= base_url() ?>img/portfolio/image-5.jpg" alt="Portfolio Image">

                    <!-- Image Zoom -->
                    <a class="prettyPhoto image_zoom" href="<?= base_url() ?>img/portfolio/image-5.jpg" title="Portfolio Image">
                        <!-- Overlay Content -->
                        <div class="item-overlay">										
                            <div class="overlay-content">
                                <h4>Enter Title Here</h4>
                                <p>Aliquam a augue suscipit, bibendum luctus neque laoreet rhoncus tellus vulputate congue</p>
                            </div>												
                        </div>	
                    </a>	<!-- End Image Zoom -->
                </div>	<!-- End Hover Overlay -->
            </li>
            <!-- IMAGE #6 -->
            <li class="col-sm-6 col-md-3 portfolio-item">
                <div class="hover-overlay"> 
                   <!-- Image Link -->
                    <img class="img-responsive" src="<?= base_url() ?>img/portfolio/image-6.jpg" alt="Portfolio Image">
                    <!-- Image Zoom -->
                    <a class="prettyPhoto image_zoom" href="<?= base_url() ?>img/portfolio/image-6.jpg" title="Portfolio Image">	
                        <!-- Overlay Content -->
                        <div class="item-overlay">											
                            <div class="overlay-content">
                                <h4>Enter Title Here</h4>
                                <p>Aliquam a augue suscipit, bibendum luctus neque laoreet rhoncus tellus vulputate congue</p>
                            </div>										
                        </div>	
                    </a>	<!-- End Image Zoom -->
                </div>	<!-- End Hover Overlay -->
            </li>
           <!-- IMAGE #7 -->
            <li class="col-sm-6 col-md-3 portfolio-item">
                <div class="hover-overlay"> 
                    <!-- Image Link -->
                    <img class="img-responsive" src="<?= base_url() ?>img/portfolio/image-7.jpg" alt="Portfolio Image">
                    <!-- Image Zoom -->
                    <a class="prettyPhoto image_zoom" href="<?= base_url() ?>img/portfolio/image-7.jpg" title="Portfolio Image">	
                        <!-- Overlay Content -->
                        <div class="item-overlay">											
                            <div class="overlay-content">
                                <h4>Enter Title Here</h4>
                                <p>Aliquam a augue suscipit, bibendum luctus neque laoreet rhoncus tellus vulputate congue</p>
                            </div>												
                        </div>	
                    </a>	<!-- End Image Zoom -->
                </div>	<!-- End Hover Overlay -->
            </li>
            <!-- IMAGE #8 -->
            <li class="col-sm-6 col-md-3 portfolio-item">
                <div class="hover-overlay"> 
                    <!-- Image Link -->
                    <img class="img-responsive" src="<?= base_url() ?>img/portfolio/image-8.jpg" alt="Portfolio Image">
                    <!-- Image Zoom -->
                    <a class="prettyPhoto image_zoom" href="<?= base_url() ?>img/portfolio/image-8.jpg" title="Portfolio Image">	
                        <!-- Overlay Content -->
                        <div class="item-overlay">											
                            <div class="overlay-content">
                                <h4>Enter Title Here</h4>
                                <p>Aliquam a augue suscipit, bibendum luctus neque laoreet rhoncus tellus vulputate congue</p>
                            </div>												
                        </div>	
                    </a>	<!-- End Image Zoom -->
                </div>	<!-- End Hover Overlay -->
            </li>
        </ul>							
    </div>		<!-- END PORTFOLIO IMAGES HOLDER -->