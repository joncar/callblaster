<div class="container">	
    <!-- TITLEBAR -->				
    <div class="row">
        <div class="col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2 section-title-thin">	
            <h4>About us in short</h4>	
            <h3>We are developing responsive and ultra modern websites to help people to promote their projects</h3>														
        </div>
    </div>	
    <div class="row">
        <!-- ABOUT TEXT -->
        <div class="col-md-4 about-txt m-bottom-50">
            <!-- Title -->
            <h4 class="h4-lg">A Little Words About Us</h4>
            <!-- Text -->							
            <p>Dapibus egestas lobortis magna phasellus euismod mauris rutrum turpis nesciunt sequi, neque. Dapibus egestas 
                lobortis porta magna phasellus nec euismod mauris rutrum turpis. Ratione sequi nesciunt, neque
            </p>
            <p>Etiam scelerisque semper justo ut convallis tempor, non convallis tincidunt, est tincidunt eros ligula, 
                dictum magna eu ligula, lacus eros varius fringilla
            </p>
            <!-- Button -->
            <a href="#" class="btn m-top-20">View Features</a>
        </div>
        <!-- ABOUT BOXES -->
        <div class="col-md-8 p-left-45 m-bottom-20">
            <div class="row">
                <div class="col-md-6">
                    <!-- ABOUT BOX #1 -->
                    <div class="abox m-bottom-30">
                        <!-- Icon -->
                        <div class="abox-icon">
                            <span class="ti-desktop"></span>	
                        </div>
                        <!-- Text -->
                        <div class="abox-txt">										
                            <h4>Responsive Layout</h4>											
                            <p>Praesent semper, lacus cursus porta, feugiat primis in vehicula pharetra ultrice tellus ligula
                                and molestie posuere potenti enim magna imperdiet felis ut lorem
                            </p>											
                        </div>
                    </div>
                    <!-- ABOUT BOX #2 -->
                    <div class="abox m-bottom-30">
                        <!-- Icon -->
                        <div class="abox-icon">
                            <span class="ti-panel"></span>
                        </div>
                        <!-- Text -->
                        <div class="abox-txt">										
                            <h4>Extremely Flexible</h4>											
                            <p>Praesent semper, lacus cursus porta, feugiat primis in vehicula pharetra ultrice tellus ligula
                                and molestie posuere potenti enim magna imperdiet felis ut lorem
                            </p>											
                        </div>
                    </div>
                </div>
                <div class="col-md-6 m-bottom-20">
                    <!-- ABOUT BOX #3 -->
                    <div class="abox m-bottom-30">
                        <!-- Icon -->
                        <div class="abox-icon">
                            <span class="ti-user"></span>
                        </div>
                        <!-- Text -->
                        <div class="abox-txt">											
                            <h4>Social Media</h4>											
                            <p>Praesent semper, lacus cursus porta, feugiat primis in vehicula pharetra ultrice tellus ligula
                                and molestie posuere potenti enim magna imperdiet felis ut lorem
                            </p>											
                        </div>
                    </div>
                    <!-- ABOUT BOX #4 -->
                    <div class="abox m-bottom-20">
                        <!-- Icon -->
                        <div class="abox-icon">
                            <span class="ti-world"></span>
                        </div>
                        <!-- Text -->
                        <div class="abox-txt">										
                            <h4>SEO Optimized</h4>											
                            <p>Praesent semper, lacus cursus porta, feugiat primis in vehicula pharetra ultrice tellus ligula
                                and molestie posuere potenti enim magna imperdiet felis ut lorem
                            </p>											
                        </div>
                    </div>
                </div>
            </div>	<!-- End row -->
        </div>	<!-- END ABOUT BOXES -->
    </div>    <!-- End row -->
</div>	   <!-- End container -->