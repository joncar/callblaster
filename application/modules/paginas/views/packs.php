<!--Banner-->
<section class="page-heading">
    <div class="title-slide">
        <div class="container">
            <div class="banner-content slide-container">									
                <div class="page-title">
                    <h3>Mike Stramond</h3>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Banner-->
<div class="page-content">					
    <!-- Breadcrumbs -->
    <div class="breadcrumbs">
        <div class="container">
            <ul>
                <li class="home"><a href="#"><i class="fa fa-home"></i> Home</a></li>
                <li><span>//</span></li>
                <li class="category-1"><a href="#">Our class</a></li>
                <li><span>//</span></li>
                <li class="category-2"><a href="#">Mike Stramond</a></li>
            </ul>
        </div>
    </div>
    <!-- End Breadcrumbs -->
    <!-- Main Content -->
    <div class="main-content class-detail">
        <div class="container">
            <div class="row">
                <div class="profile-details">
                    <div class="col-lg-6 col-md-8 col-sm-12 col-xs-12">
                        <section class="banner-details">
                            <div id="carousel" class="carousel slide" data-ride="carousel">											
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner" role="listbox">
                                    <div class="item active">
                                        <img src="<?= base_url() ?>img/bg-trainer-profile.jpg" alt="">
                                    </div>

                                    <div class="item">
                                        <img src="<?= base_url() ?>img/bg-trainer-profile-2.jpg" alt="">
                                    </div>

                                    <div class="item">
                                        <img src="<?= base_url() ?>img/bg-trainer-profile-3.jpg" alt="">
                                    </div>													
                                </div>

                                <!-- Left and right controls -->
                                <a class="left carousel-control" href="#carousel" role="button" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="right carousel-control" href="#carousel" role="button" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </section>											
                        <!-- End Banner Content -->

                        <!--  Training Experience -->
                        <div class="content-pages">
                            <section class="experience">
                                <div class="experience-content">
                                    <div class="experience-title">
                                        <h5>Training Experience</h5>													
                                    </div>
                                    <div class="experience-main experience-main-plus" data-active-first="yes" >
                                        <div class="experience-spoiler">
                                            <div class="experience-details-title">
                                                <i class="fa fa-plus"></i>
                                                Lorem ipsum dolor sit amet
                                                <span class="experience-details-collapse"></span>
                                            </div>
                                            <div class="experience-details-content">Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips amets. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam. sit voluptatem accusantium. Doloremque laudantium, totam rem aperiamsit voluptatem accusantium </div>
                                        </div>
                                    </div>
                                    <div class="experience-main experience-main-plus">
                                        <div class="experience-spoiler">
                                            <div class="experience-details-title">
                                                <i class="fa fa-plus"></i>
                                                Lorem ipsum dolor sit amet
                                                <span class="experience-details-collapse"></span>
                                            </div>
                                            <div class="experience-details-content">Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips amets. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam. sit voluptatem accusantium. Doloremque laudantium, totam rem aperiamsit voluptatem accusantium </div>
                                        </div>
                                    </div>
                                    <div class="experience-main experience-main-plus">
                                        <div class="experience-spoiler">
                                            <div class="experience-details-title">
                                                <i class="fa fa-plus"></i>
                                                Lorem ipsum dolor sit amet
                                                <span class="experience-details-collapse"></span>
                                            </div>
                                            <div class="experience-details-content">Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips amets. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam. sit voluptatem accusantium. Doloremque laudantium, totam rem aperiamsit voluptatem accusantium </div>
                                        </div>
                                    </div>
                                </div>

                            </section>
                        </div>
                        <!-- End Training Experience -->																		
                    </div>		
                    <div class="col-lg-6 col-md-4 col-sm-12 col-xs-12">
                        <section class="profile">
                            <div class="profile-title">
                                <div class="row">
                                    <div class="img-profile1">
                                        <img src="<?= base_url() ?>img/img-protile.png" alt="">
                                    </div>
                                    <div class="profile-info">
                                        <div class="profile-info-top">
                                            <span>Age</span>
                                            <span class="profile-info-right">: 32 year old</span>														
                                        </div>
                                        <div class="profile-info-bottom">
                                            <span>Training Experience</span>
                                            <span class="profile-info-right">: 12 year</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="profile-content">
                                <div class="profile-icon">
                                    <div class="rating">
                                        <span><i class="fa fa-star"></i></span>
                                        <span><i class="fa fa-star"></i></span>
                                        <span><i class="fa fa-star"></i></span>
                                        <span><i class="fa fa-star"></i></span>
                                        <span><i class="fa fa-star"></i></span>
                                    </div>														
                                </div>
                                <div class="profile-text">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut
                                        laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, 
                                        sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                                    </p>
                                    <div class="share">
                                        <div class="share-title">
                                            <h5>Share This profile</h5>										
                                        </div>
                                        <div class="social-icon">
                                            <a href="#"><i class="fa fa-facebook"></i></a>
                                            <a href="#"><i class="fa fa-twitter"></i></a>
                                            <a href="#"><i class="fa fa-google-plus"></i></a>
                                            <a href="#"><i class="fa fa-pinterest"></i></a>
                                            <a href="#" class="in"><i class="fa fa-linkedin"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>	
                        <section class="experience skills">
                            <div class="experience-content">
                                <div class="experience-title">
                                    <h5>Training Skills</h5>													
                                </div>
                                <ul>
                                    <li class="skill skill-top">
                                        <span class="skill-title">Sed Molestie augue</span>
                                        <span class="skill-percent skill-percent-callout">95%</span>
                                        <div data-value="95" class="progress-indicator">
                                            <div class="bg-red"></div>
                                        </div>
                                    </li>
                                    <li class="skill">
                                        <span class="skill-title">Praesent Id Metus Massa</span>
                                        <span class="skill-percent skill-percent-callout">95%</span>
                                        <div data-value="95" class="progress-indicator">
                                            <div class="bg-red"></div>
                                        </div>
                                    </li>
                                    <li class="skill">
                                        <span class="skill-title">Nulla At Nulla</span>
                                        <span class="skill-percent skill-percent-callout">95%</span>
                                        <div data-value="95" class="progress-indicator">
                                            <div class="bg-red"></div>
                                        </div>
                                    </li>
                                    <li class="skill">
                                        <span class="skill-title">Proin Quis Tortor Orci</span>
                                        <span class="skill-percent skill-percent-callout">95%</span>
                                        <div data-value="95" class="progress-indicator">
                                            <div class="bg-red"></div>
                                        </div>
                                    </li>
                                    <li class="skill">
                                        <span class="skill-title">Training Technical</span>
                                        <span class="skill-percent skill-percent-callout">95%</span>
                                        <div data-value="95" class="progress-indicator">
                                            <div class="bg-red"></div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </section>
                    </div>
                </div>
                <!--End Profile Details-->
                <!--Meet Trainer-->
                <section class="meet-trainer">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="title-page">
                                    <h3>Meet Other Trainers</h3>
                                    <p>Working from home meant we could vary snack and coffee breaks, change our desks or view, goof off, drink on the job, even spend the day in pajamas</p>
                                </div>
                                <section id="our-trainers" class="our-trainers">
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="product-image-wrapper">
                                            <div class="product-content">
                                                <div class="product-image">
                                                    <a href="#"><img alt="" src="<?= base_url() ?>img/our_trainers_1.png"></a>
                                                </div>
                                                <div class="info-products">
                                                    <div class="img-trainers">
                                                        <img src="<?= base_url() ?>img/boxing-icon-1.png" alt="">
                                                    </div>
                                                    <div class="product-name" >
                                                        <a href="#">Jonh Mike</a>
                                                        <div class="product-bottom"></div>
                                                    </div>
                                                    <div class="product-info">																										
                                                        <p>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Lorem ipsum dolor sit amet, consectetur adipiscing elit.  </p>
                                                    </div>												
                                                    <div class="actions">
                                                        <ul>
                                                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>												
                                            </div>																																											
                                        </div>																																											
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="product-image-wrapper">
                                            <div class="product-content">
                                                <div class="product-image">
                                                    <a href="#"><img alt="" src="<?= base_url() ?>img/our_class_4.png"></a>
                                                </div>
                                                <div class="info-products">
                                                    <div class="img-trainers">
                                                        <img src="<?= base_url() ?>img/boxing-icon-1.png" alt="">
                                                    </div>
                                                    <div class="product-name" >
                                                        <a href="#">Jonh Mike</a>
                                                        <div class="product-bottom"></div>
                                                    </div>
                                                    <div class="product-info">																										
                                                        <p>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Lorem ipsum dolor sit amet, consectetur adipiscing elit.  </p>
                                                    </div>												
                                                    <div class="actions">
                                                        <ul>
                                                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>												
                                            </div>																																											
                                        </div>																																											
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="product-image-wrapper">
                                            <div class="product-content">
                                                <div class="product-image">
                                                    <a href="#"><img alt="" src="<?= base_url() ?>img/our_class_2.png"></a>
                                                </div>
                                                <div class="info-products">
                                                    <div class="img-trainers">
                                                        <img src="<?= base_url() ?>img/boxing-icon-1.png" alt="">
                                                    </div>
                                                    <div class="product-name" >
                                                        <a href="#">Jonh Mike</a>
                                                        <div class="product-bottom"></div>
                                                    </div>
                                                    <div class="product-info">																										
                                                        <p>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Lorem ipsum dolor sit amet, consectetur adipiscing elit.  </p>
                                                    </div>												
                                                    <div class="actions">
                                                        <ul>
                                                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>												
                                            </div>																																											
                                        </div>																																											
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </section>
                <!--End Meet Trainer-->
            </div>
        </div>
    </div>
    <!-- Main Content -->
</div>
